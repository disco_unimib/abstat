#! /bin/bash

#cloning repo and checkout branch
function clone(){
	mkdir virtuoso
	cd virtuoso
	git clone https://github.com/openlink/virtuoso-opensource
	cd virtuoso-opensource
	git checkout develop/7
}


function dependencies(){
	sudo apt-get update
	printf "\n----- INSTALLING autoconf ------"
	sudo apt-get install -y autoconf
	printf "\n----- INSTALLING automake ------"
	sudo apt-get install -y automake
	printf "\n----- INSTALLING libtool ------"
	sudo apt-get install -y libtool
	printf "\n----- INSTALLING flex ------"
	sudo apt-get install -y flex
	printf "\n----- INSTALLING bison ------"
	sudo apt-get install -y bison
	printf "\n----- INSTALLING gperf ------"
	sudo apt-get install -y gperf
	printf "\n----- INSTALLING gawk ------"
	sudo apt-get install -y gawk
	printf "\n----- INSTALLING m4 ------"
	sudo apt-get install -y m4
	printf "\n----- INSTALLING make ------"
	sudo apt-get install -y make
	printf "\n----- INSTALLING openssl ------"
	sudo apt-get install -y openssl
	printf "\n----- INSTALLING libssl-dev  ------"
	sudo apt-get install -y libssl-dev 
}


function building(){
	cd $virtuoso_dir
	mkdir virtuoso-bin
	cd virtuoso-opensource
	./autogen.sh
	./configure --prefix=$virtuoso_dir"/virtuoso-bin"
	make
	make install
	mv $virtuoso_dir"/virtuoso-bin/var/lib/virtuoso/db/virtuoso.ini" $virtuoso_dir"/virtuoso-bin/bin"
}

relative_path=`dirname $0`
root=`cd $relative_path;pwd`
virtuoso_dir=`cd $relative_path;pwd`"/virtuoso"


clone
dependencies
building
