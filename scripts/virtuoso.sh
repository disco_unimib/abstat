#! /bin/bash

function bulk_load(){
	#$1 = path, $2 = filePattern, $3 = graphName
	$isql localhost:1111 dba dba exec="ld_dir ('$1', '$2', '$3');rdf_loader_run();checkpoint;exit;"
}

function smart_load_ttl(){
	#$1 = filePath, $2 = graphName
	./virtuoso_smart_load_ttl.sh dba $1 $2
}

function smart_load_bulk(){
	logfile=$(basename -- "$0")
	PASS=dba
	#$2 = path, $3 = filePattern, $4 = graphName
	p=$1
	f=$2
	g=$3

	rm -rf $p/bad.nt

	# Usage
	if [ -z "$PASS" -o -z "$f" -o -z "$g" ]
	then
	  echo "Usage: $logfile [password] [path] [filePattern] [graph-iri]"
	  exit
	fi

	if [ ! -f "$p/$f" ]
	then
	    echo "$p/$f does not exists"
	    exit
	fi

	# Your port here
	PORT=1111  #`inifile -f dbpedia.ini -s Parameters -k ServerPort`
	if test -z "$PORT"
	then
	    echo "Cannot find INI and inifile command"
	    exit
	fi

	# Initial run
	$isql $PORT dba $PASS verbose=on banner=off prompt=off echo=ON errors=stdout exec="ld_dir ('$p', '$f', '$g');rdf_loader_run();checkpoint;exit;" 
	$isql $PORT dba $PASS verbose=on banner=off prompt=off echo=ON errors=stdout exec="select ll_error from db.dba.load_list where ll_graph = '$g';exit;" > $p/$logfile.log

	# If disconnect etc.
	if [ $? != 0 ]
	then
	    echo "An error occurred, please check $p/$logfile.log"
	    exit
	fi

	# Check for error
	line_no=`grep 'TURTLE RDF loader' $p/$logfile.log | awk '{ match ($logfile, /line [0-9]+/, x) ; match (x[0], /[0-9]+/, y); print y[0] }'`
	newf=_$f
	inx=1


	# Error recovery
	while [ ! -z "$line_no" ]
	do
	    cat $p/$f |  awk "BEGIN { i = 1 } { if (i==$line_no) { print \$logfile; exit; } i = i + 1 }"  >> $p/bad.nt
	    echo "Retrying from line $line_no"
	    cat $p/$f |  awk "BEGIN { i = 0 } { if (i>=$line_no) print \$logfile; i = i + 1 }"  > $p/tmp.nt
	    mv $p/tmp.nt $p/$newf
	    f=$newf
	    mv $p/$logfile.log $p/$logfile.log.$inx
	    # Run the recovered part 
	    $isql $PORT dba $PASS verbose=on banner=off prompt=off echo=ON errors=stdout exec="delete from DB.DBA.load_list where ll_graph = '$g';checkpoint;exit;"
	    $isql $PORT dba $PASS verbose=on banner=off prompt=off echo=ON errors=stdout exec="ld_dir ('$p', '$f', '$g');rdf_loader_run();checkpoint;exit;" 
	    $isql $PORT dba $PASS verbose=on banner=off prompt=off echo=ON errors=stdout exec="select ll_error from db.dba.load_list where ll_graph = '$g';exit;" > $p/$logfile.log

	    if [ $? != 0 ]
	    then
		echo "An error occurred, please check $p/$logfile.log"
		exit
	    fi
	   line_no=`grep 'TURTLE RDF loader' $p/$logfile.log | awk '{ match ($logfile, /line [0-9]+/, x) ; match (x[0], /[0-9]+/, y); print y[0] }'`
           rm -rf $p/$logfile.log.$inx
	   inx=`expr $inx + 1`
	done

	# Delete tmp files
        rm -rf $p/$logfile.log
	rm -rf $p/$newf



}

function clear_load_list(){
	#$1 = graphName
	$isql localhost:1111 dba dba exec="delete from DB.DBA.load_list where ll_graph = '$1';checkpoint;exit;"
}

function delete_graph(){
	#$1 = graphName
	clear_load_list $1
	$isql localhost:1111 dba dba exec="log_enable(3,1);SPARQL CLEAR GRAPH  <$1>;checkpoint;exit;"
}

function create_ruleset(){
	#$1 = ruleName, $2 = graphName
	delete_ruleset $1 $2
	$isql localhost:1111 dba dba exec="rdfs_rule_set('$1', '$2');checkpoint;exit;"
}

function delete_ruleset(){
	#$1 = ruleName, $2 = graphName
	$isql localhost:1111 dba dba exec="rdfs_rule_set('$1','$2', 1);checkpoint;exit;"

}

relative_path=`dirname $0`
root=`cd $relative_path/..;pwd`
isql=$root"/virtuoso/virtuoso-bin/bin/isql"

case "$1" in
        bulk_load)
			bulk_load  $2 $3 $4
            ;;
	smart_load_ttl)
			smart_load_ttl $2 $3
            ;;
	smart_load_bulk)
			smart_load_bulk $2 $3 $4
            ;;
        clear_load_list)
			clear_load_list $2
            ;;
        delete_graph)
			delete_graph $2
            ;;
	create_ruleset)
			create_ruleset $2 $3
	;;
	delete_ruleset)
			delete_ruleset $2 $3
	;;
	*)
        	echo "Usage: virtuoso bulk_load | smart_load_ttl | smart_load_bulk | clear_load_list | delete_graph | create_ruleset | delete_ruleset"
			;;
esac
