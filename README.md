
# ABSTAT (Back-end Application)
This repository contains the back-end module, which provides API endpoints, handles and dispatches requests, and interfaces with the databases. For profiling core algorithms, please refer to the [ABSTAt-HD repository](https://bitbucket.org/disco_unimib/abstat-distributed).

## Prerequisites for Running ABSTAT

Tested on **Ubuntu 16.04 LTS**  
ABSTAT depends on the following software:

```
* Nginx
* MongoDB
* Java
* Maven
* Virtuoso
```

---

## Cloning the Repository, Installing, and Testing ABSTAT

Clone the repository and navigate into the project directory:

```bash
$ git clone https://bitbucket.org/disco_unimib/abstat
$ cd abstat
```

Run the installation script to install necessary dependencies:

```bash
$ sudo ./abstat.sh install
```
> **Note:** If you do not have the software requirements installed.  
If you are running ABSTAT on a cluster, include the `-distributed` option.

Personalize the `application.properties` file, for instance, if you want a single-machine backend without usage restrictions, update the following key-values:

- `spring.profiles.active=single-machine`
- `restriction.policy=full-open`

Configure the Nginx and application ports (note that this will overwrite the previous Nginx configuration):

```bash
$ sudo ./abstat.sh config nginx_port application_port
```

Finally, start the environment and run the build and test script:

```bash
$ sudo ./abstat.sh startenv
$ ./build-and-test.sh
```

---

## Controlling ABSTAT

ABSTAT can be controlled using the script `abstat.sh` from the root of the repository with the following commands:

### Build ABSTAT
```bash
$ ./abstat.sh build
```

### Start and Stop Environment
```bash
$ sudo ./abstat.sh startenv   # Starts the ABSTAT environment
$ ./abstat.sh start           # Starts ABSTAT
$ sudo ./abstat.sh stopenv    # Stops the ABSTAT environment
```

## References

For a detailed explanation of ABSTAT’s methodologies and applications, refer to the following papers:

- *Principe, R. A. A., Spahiu, B., Palmonari, M., Rula, A., De Paoli, F., & Maurino, A. (2018). ABSTAT 1.0: Compute, manage and share semantic profiles of RDF knowledge graphs. In The Semantic Web: ESWC 2018 Satellite Events: ESWC 2018 Satellite Events, Heraklion, Crete, Greece, June 3-7, 2018, Revised Selected Papers 15 (pp. 170-175). Springer International Publishing.*
- *Alva Principe, R. A., Maurino, A., Palmonari, M., Ciavotta, M., & Spahiu, B. (2021). ABSTAT-HD: a scalable tool for profiling very large knowledge graphs. The VLDB Journal, 1-26.*
---


## License

GNU Affero General Public License v3.0