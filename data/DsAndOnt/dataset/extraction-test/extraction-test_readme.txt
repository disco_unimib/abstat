# Section
Line number

# subject type declaration
1

# object type declaration 
15

# Subj-Objs Concept Property owl:Thing
55

# Subj-Objs Concept Property Concept
66

# Subj-Objs Concept Property Datatype
77

# Subj-Objs Concept Property rdfs:Literal
88

# Subj-Objs owl:Thing Property owl:Thing
99

# Subj-Objs  owl:Thing Property Concept
104

# Subj-Objs owl:Thing Property Datatype
109

# Subj-Objs owl:Thing Property rdfs:Literal
114

# Subjs-Obj Concept Property owl:Thing
119

# Subjs-Obj Concept Property Concept
123

# Subjs-Obj Concept Property Datatype
127

# Subjs-Obj Concept Property rdfs:Literal
131

# Subjs-Obj owl:Thing Property owl:Thing
135

# Subjs-Obj  owl:Thing Property Concept 
139

# Subjs-Obj owl:Thing Property Datatype
143

# Subjs-Obj owl:Thing Property rdfs:Literal
147

# differenType/subType/superType violations => should not affect the results of the tests
151

# triples for sort/limit/offset tests
199

# abstat ???
<http://dbpedia.org/resource/Artist_OK> <http://xmlns.com/foaf/0.1/friend> <http://dbpedia.org/resource/Artist_KO> .

