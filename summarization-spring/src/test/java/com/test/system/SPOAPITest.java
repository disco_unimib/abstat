package com.test.system;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.model.SubmitConfig;
import com.service.SubmitConfigService;
import com.start.abstat.AbstatApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AbstatApplication.class)
@AutoConfigureMockMvc
public class SPOAPITest {

    @Autowired
    private MockMvc mockMvc;
	@Autowired
	private SubmitConfigService service;
	  
    private String idSummary;
	  
	@Before
	public void init() {
		List<SubmitConfig> list = service.listSubmitConfig();
		for(SubmitConfig config : list)
			if(config.getDsId().contentEquals("system-test_dataset") && 
					config.getListOntId().get(0).equals("system-test_ontology") && 
					config.isTipoMinimo() && 
					config.isInferences() && 
					config.isCardinalita() && 
					config.isPropertyMinimaliz()) 
				idSummary=config.getId();
	  }

	
    @Test
    public void shouldWorkWithSubject() throws Exception {
        this.mockMvc.perform(get("/api/v1/SPO")
        		.param("summary", idSummary)
        		.param("position", "subject"))
                .andExpect(content().string(StringMatcher.containsStringNTimes("\"result\"", 16)));
    }
    
    @Test
    public void shouldWorkWithPredicate() throws Exception {
        this.mockMvc.perform(get("/api/v1/SPO")
        		.param("summary", idSummary)
        		.param("position", "predicate"))
                .andExpect(content().string(StringMatcher.containsStringNTimes("\"result\"", 16)));
    }
    
    @Test
    public void shouldWorkWithObject() throws Exception {
        this.mockMvc.perform(get("/api/v1/SPO")
        		.param("summary", idSummary)
        		.param("position", "object"))
                .andExpect(content().string(StringMatcher.containsStringNTimes("\"result\"", 16)));
    }
    
    
    //TO-DO 
    //load a second summary and write the same tests without "summary" parameter
    
}
