package com.test.system;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.hp.hpl.jena.sparql.engine.http.QueryEngineHTTP;
import com.model.SubmitConfig;
import com.service.SubmitConfigService;
import com.start.abstat.AbstatApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AbstatApplication.class)
@AutoConfigureMockMvc
public class VirtuosoDeleteTest {
	
	@Autowired
	private MockMvc mockMvc;
	@Autowired
	SubmitConfigService submitconfigService;
	
	String idSummary;
	static boolean flag = true;
	
	private String dataset;
	private String ontology;

	@Before
	public void init() throws Exception {
		dataset = "extraction-test";
		ontology = "dbpedia_2014";

		//flag is used to avoid multiexecution of this code
		if(flag) {
			for(SubmitConfig config : submitconfigService.listSubmitConfig()) {
				if(config.getDsId().equals("extraction-test_dataset")) {
					idSummary=config.getId();
				}
			}

			flag = false;
			mockMvc.perform(post("/submitconfig/deleteDir/" + idSummary))
			.andDo(print()).andExpect(status().is3xxRedirection());
			mockMvc.perform(post("/dataset/delete/extraction-test_dataset"))
			.andDo(print()).andExpect(status().is3xxRedirection());
			mockMvc.perform(post("/ontology/delete/extraction-test_ontology"))
			.andDo(print()).andExpect(status().is3xxRedirection());
		}
	}
	
	@Test
	public void shouldHaveDeletedDataset() throws Exception {
		String sparql = "ASK WHERE { GRAPH <" + dataset + "> { ?s ?p ?o } }";
		QueryEngineHTTP request = new QueryEngineHTTP ("http://localhost:8890/sparql", sparql);
		// request.execAsk() = true => graph still present
		assertFalse(request.execAsk());
		request.close();
	}
	
	@Test
	public void shouldHaveDeletedOntology() throws Exception {
		String sparql = "ASK WHERE { GRAPH <" + ontology + "> { ?s ?p ?o } }";
		QueryEngineHTTP request = new QueryEngineHTTP ("http://localhost:8890/sparql", sparql);
		// request.execAsk() = true => graph still present
		assertFalse(request.execAsk());
		request.close();
	}
	
}
