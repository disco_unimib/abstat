package com.test.system;

import static org.hamcrest.Matchers.stringContainsInOrder;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import com.start.abstat.AbstatApplication;



@RunWith(SpringRunner.class)
@SpringBootTest(classes = AbstatApplication.class)
@AutoConfigureMockMvc
public class TriplesExtractorAPIGroupedTest {
	@Autowired
	private MockMvc mockMvc;

	private String dataset;
	private String ontology;
	private String dboMusicalArtist;
	private String dboArtist;
	private String owlThing;
	private String xsdInteger;
	private String rdfsLiteral;
	private String dboChild;
	private String dboAge;
	private String foafName;
	private String concept;
	private String datatype;
	private String maxDirect;
	private String maxInverse;

	@Before
	public void init() {
		dataset = "extraction-test";
		ontology = "dbpedia_2014";
		dboMusicalArtist = "http://dbpedia.org/ontology/MusicalArtist";
		dboArtist = "http://dbpedia.org/ontology/Artist";
		owlThing = "http://www.w3.org/2002/07/owl#Thing";
		xsdInteger = "http://www.w3.org/2001/XMLSchema#integer";
		rdfsLiteral = "http://www.w3.org/2000/01/rdf-schema#Literal";
		dboChild = "http://dbpedia.org/ontology/child";
		dboAge = "http://dbpedia.org/ontology/age";
		foafName = "http://xmlns.com/foaf/0.1/name";
		concept = "ObjectAKP";
		datatype = "DatatypeAKP";
		maxDirect = "MaxSubjObjs";
		maxInverse = "MaxSubjsObj";
	}

	/******************************************** showTriples **************************************************/
	/*------------------------------------------- output fields -----------------------------------------------*/

	@Test
	public void shouldShowTriplesFieldsMaxDirect() throws Exception{
		mockMvc.perform(get("/api/v1/groupedExtractor")
				.param("subj", dboMusicalArtist)
				.param("pred", dboChild)
				.param("obj", owlThing)
				.param("dataset", dataset)
				.param("ontology", ontology)
				.param("akpType", concept)
				.param("cardinalityType", maxDirect)
				.param("predictedCardinality", "1")
				.param("limit", "100")
				.param("offset", "0")
				.param("sort", "true")
				.param("showTriples", "true"))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"head\":", 1)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"vars\":", 1)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"results\":", 1)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"bindings\":", 1)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"subj\":", 2)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"pred\":", 2)))
		//.andExpect(content().string(StringMatcher.containsStringNTimes("\"nobjs\":", 2)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"objs\":", 2)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"value\":", 8)));
	}

	@Test
	public void shouldShowTriplesFieldsMaxInverse() throws Exception{
		mockMvc.perform(get("/api/v1/groupedExtractor")
				.param("subj", dboMusicalArtist)
				.param("pred", dboChild)
				.param("obj", owlThing)
				.param("dataset", dataset)
				.param("ontology", ontology)
				.param("akpType", concept)
				.param("cardinalityType", maxInverse)
				.param("predictedCardinality", "1")
				.param("limit", "100")
				.param("offset", "0")
				.param("sort", "true")
				.param("showTriples", "true"))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"head\":", 1)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"vars\":", 1)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"results\":", 1)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"bindings\":", 1)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"obj\":", 2)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"pred\":", 2)))
		//.andExpect(content().string(StringMatcher.containsStringNTimes("\"nsubjs\":", 2)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"subjs\":", 2)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"value\":", 8)));
	}

	/*--------------------------------- check results MaxSubjObjs -------------------------------------------*/
	@Test
	public void shouldExtractTriplesConceptThingMaxDirect() throws Exception{
		mockMvc.perform(get("/api/v1/groupedExtractor")
				.param("subj", dboMusicalArtist)
				.param("pred", dboChild)
				.param("obj", owlThing)
				.param("dataset", dataset)
				.param("ontology", ontology)
				.param("akpType", concept)
				.param("cardinalityType", maxDirect)
				.param("predictedCardinality", "1")
				.param("limit", "100")
				.param("offset", "0")
				.param("sort", "true")
				.param("showTriples", "true"))
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Artist", 0))) // check absence of supertype violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Guitarist", 0))) // check absence of subtype violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("_OK", 0))) // check absence of false violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"subj\":", 2))) // 2 subj => 2 results
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/MusicalArtist_KO_1\"", 1))) // subj1
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/MusicalArtist_KO_2\"", 1))) // subj2
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"" + dboChild + "\"", 2))) // pred
		//.andExpect(content().string(StringMatcher.containsStringNTimes("\"4\"", 2))) // nobjs
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/MusicalArtist_KO_1_UndefinedChild_1\"", 1)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/MusicalArtist_KO_1_UndefinedChild_2\"", 1)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/MusicalArtist_KO_2_UndefinedChild_1\"", 1)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/MusicalArtist_KO_2_UndefinedChild_2\"", 1)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/MusicalArtist_KO_12_UndefinedChild_1\"", 2)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/MusicalArtist_KO_12_UndefinedChild_2\"", 2)))
		;
	}

	@Test
	public void shouldExtractTriplesConceptConceptMaxDirect() throws Exception{
		mockMvc.perform(get("/api/v1/groupedExtractor")
				.param("subj", dboMusicalArtist)
				.param("pred", dboChild)
				.param("obj", dboMusicalArtist)
				.param("dataset", dataset)
				.param("ontology", ontology)
				.param("akpType", concept)
				.param("cardinalityType", maxDirect)
				.param("predictedCardinality", "1")
				.param("limit", "100")
				.param("offset", "0")
				.param("sort", "true")
				.param("showTriples", "true"))
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Artist", 0))) // check absence of supertype violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Guitarist", 0))) // check absence of subtype violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("Undefined", 0))) // check absence of untyped violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("_OK", 0))) // check absence of false violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"subj\":", 2))) // 2 subj => 2 results
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/MusicalArtist_KO_1\"", 1))) // subj1
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/MusicalArtist_KO_2\"", 1))) // subj2
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"" + dboChild + "\"", 2))) // pred
		//.andExpect(content().string(StringMatcher.containsStringNTimes("\"4\"", 2))) // nobjs
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/MusicalArtist_KO_1_Child_1\"", 1)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/MusicalArtist_KO_1_Child_2\"", 1)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/MusicalArtist_KO_2_Child_1\"", 1)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/MusicalArtist_KO_2_Child_2\"", 1)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/MusicalArtist_KO_12_Child_1\"", 2)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/MusicalArtist_KO_12_Child_2\"", 2)))
		;
	}

	@Test
	public void shouldExtractTriplesConceptDatatypeMaxDirect() throws Exception{
		mockMvc.perform(get("/api/v1/groupedExtractor")
				.param("subj", dboMusicalArtist)
				.param("pred", dboAge)
				.param("obj", xsdInteger)
				.param("dataset", dataset)
				.param("ontology", ontology)
				.param("akpType", datatype)
				.param("cardinalityType", maxDirect)
				.param("predictedCardinality", "1")
				.param("limit", "100")
				.param("offset", "0")
				.param("sort", "true")
				.param("showTriples", "true"))
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Artist", 0))) // check absence of supertype violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Guitarist", 0))) // check absence of subtype violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("Undefined", 0))) // check absence of untyped violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("_OK", 0))) // check absence of false violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"subj\":", 2))) // 2 subj => 2 results
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/MusicalArtist_KO_1\"", 1))) // subj1
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/MusicalArtist_KO_2\"", 1))) // subj2
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"" + dboAge + "\"", 2))) // pred
		//.andExpect(content().string(StringMatcher.containsStringNTimes("\"4\"", 2))) // nobjs
		.andExpect(content().string(StringMatcher.containsStringNTimes("21", 1)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("22", 1)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("23", 1)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("24", 1)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("333", 2)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("666", 2)))
		;
	}

	@Test
	public void shouldExtractTriplesConceptLiteralMaxDirect() throws Exception{
		mockMvc.perform(get("/api/v1/groupedExtractor")
				.param("subj", dboMusicalArtist)
				.param("pred", foafName)
				.param("obj", rdfsLiteral)
				.param("dataset", dataset)
				.param("ontology", ontology)
				.param("akpType", datatype)
				.param("cardinalityType", maxDirect)
				.param("predictedCardinality", "1")
				.param("limit", "100")
				.param("offset", "0")
				.param("sort", "true")
				.param("showTriples", "true"))
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Artist", 0))) // check absence of supertype violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Guitarist", 0))) // check absence of subtype violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("Undefined", 0))) // check absence of untyped violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("_OK", 0))) // check absence of false violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"subj\":", 2))) // 2 subj => 2 results
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/MusicalArtist_KO_1\"", 1))) // subj1
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/MusicalArtist_KO_2\"", 1))) // subj2
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"" + foafName + "\"", 2))) // pred
		//.andExpect(content().string(StringMatcher.containsStringNTimes("\"4\"", 2))) // nobjs
		.andExpect(content().string(StringMatcher.containsStringNTimes("MusicalArtist_KO_1 Name_1", 1)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("MusicalArtist_KO_1 Name_2", 1)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("MusicalArtist_KO_2 Name_1", 1)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("MusicalArtist_KO_2 Name_2", 1)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("MusicalArtist_KO_12 Name_1", 2)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("MusicalArtist_KO_12 Name_2", 2)))
		;
	}


	@Test
	public void shouldExtractTriplesThingThingMaxDirect() throws Exception{
		mockMvc.perform(get("/api/v1/groupedExtractor")
				.param("subj", owlThing)
				.param("pred", dboChild)
				.param("obj", owlThing)
				.param("dataset", dataset)
				.param("ontology", ontology)
				.param("akpType", concept)
				.param("cardinalityType", maxDirect)
				.param("predictedCardinality", "1")
				.param("limit", "100")
				.param("offset", "0")
				.param("sort", "true")
				.param("showTriples", "true"))
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Artist", 0))) // check absence of typed violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("/MusicalArtist", 0))) // check absence of typed violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Guitarist", 0))) // check absence of typed violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("_OK", 0))) // check absence of false violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"subj\":", 2))) // 2 subj => 2 results
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/UndefinedSubj_KO_1\"", 1))) // subj1
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/UndefinedSubj_KO_2\"", 1))) // subj2
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"" + dboChild + "\"", 2))) // pred
		//.andExpect(content().string(StringMatcher.containsStringNTimes("\"4\"", 2))) // nobjs
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/UndefinedSubj_KO_1_UndefinedChild_1\"", 1)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/UndefinedSubj_KO_1_UndefinedChild_2\"", 1)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/UndefinedSubj_KO_2_UndefinedChild_1\"", 1)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/UndefinedSubj_KO_2_UndefinedChild_2\"", 1)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/UndefinedSubj_KO_12_UndefinedChild_1\"", 2)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/UndefinedSubj_KO_12_UndefinedChild_2\"", 2)))
		;
	}

	@Test
	public void shouldExtractTriplesThingConceptMaxDirect() throws Exception{
		mockMvc.perform(get("/api/v1/groupedExtractor")
				.param("subj", owlThing)
				.param("pred", dboChild)
				.param("obj", dboMusicalArtist)
				.param("dataset", dataset)
				.param("ontology", ontology)
				.param("akpType", concept)
				.param("cardinalityType", maxDirect)
				.param("predictedCardinality", "1")
				.param("limit", "100")
				.param("offset", "0")
				.param("sort", "true")
				.param("showTriples", "true"))
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Artist", 0))) // check absence of typed violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("/MusicalArtist", 0))) // check absence of typed violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Guitarist", 0))) // check absence of typed violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("_OK", 0))) // check absence of false violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"subj\":", 2))) // 2 subj => 2 results
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/UndefinedSubj_KO_1\"", 1))) // subj1
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/UndefinedSubj_KO_2\"", 1))) // subj2
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"" + dboChild + "\"", 2))) // pred
		//.andExpect(content().string(StringMatcher.containsStringNTimes("\"4\"", 2))) // nobjs
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/UndefinedSubj_KO_1_Child_1\"", 1)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/UndefinedSubj_KO_1_Child_2\"", 1)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/UndefinedSubj_KO_2_Child_1\"", 1)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/UndefinedSubj_KO_2_Child_2\"", 1)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/UndefinedSubj_KO_12_Child_1\"", 2)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/UndefinedSubj_KO_12_Child_2\"", 2)))
		;
	}

	@Test
	public void shouldExtractTriplesThingDatatypeMaxDirect() throws Exception{
		mockMvc.perform(get("/api/v1/groupedExtractor")
				.param("subj", owlThing)
				.param("pred", dboAge)
				.param("obj", xsdInteger)
				.param("dataset", dataset)
				.param("ontology", ontology)
				.param("akpType", datatype)
				.param("cardinalityType", maxDirect)
				.param("predictedCardinality", "1")
				.param("limit", "100")
				.param("offset", "0")
				.param("sort", "true")
				.param("showTriples", "true"))
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Artist", 0))) // check absence of typed violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("/MusicalArtist", 0))) // check absence of typed violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Guitarist", 0))) // check absence of typed violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("_OK", 0))) // check absence of false violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"subj\":", 2))) // 2 subj => 2 results
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/UndefinedSubj_KO_1\"", 1))) // subj1
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/UndefinedSubj_KO_2\"", 1))) // subj2
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"" + dboAge + "\"", 2))) // pred
		//.andExpect(content().string(StringMatcher.containsStringNTimes("\"4\"", 2))) // nobjs
		.andExpect(content().string(StringMatcher.containsStringNTimes("91", 1)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("92", 1)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("93", 1)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("94", 1)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("333", 2)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("666", 2)))
		;
	}

	@Test
	public void shouldExtractTriplesThingLiteralMaxDirect() throws Exception{
		mockMvc.perform(get("/api/v1/groupedExtractor")
				.param("subj", owlThing)
				.param("pred", foafName)
				.param("obj", rdfsLiteral)
				.param("dataset", dataset)
				.param("ontology", ontology)
				.param("akpType", datatype)
				.param("cardinalityType", maxDirect)
				.param("predictedCardinality", "1")
				.param("limit", "100")
				.param("offset", "0")
				.param("sort", "true")
				.param("showTriples", "true"))
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Artist", 0))) // check absence of typed violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("/MusicalArtist", 0))) // check absence of typed violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Guitarist", 0))) // check absence of typed violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("_OK", 0))) // check absence of false violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"subj\":", 2))) // 2 subj => 2 results
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/UndefinedSubj_KO_1\"", 1))) // subj1
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/UndefinedSubj_KO_2\"", 1))) // subj2
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"" + foafName + "\"", 2))) // pred
		//.andExpect(content().string(StringMatcher.containsStringNTimes("\"4\"", 2))) // nobjs
		.andExpect(content().string(StringMatcher.containsStringNTimes("UndefinedSubj_KO_1 Name_1", 1)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("UndefinedSubj_KO_1 Name_2", 1)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("UndefinedSubj_KO_2 Name_1", 1)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("UndefinedSubj_KO_2 Name_2", 1)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("UndefinedSubj_KO_12 Name_1", 2)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("UndefinedSubj_KO_12 Name_2", 2)))
		;
	}

	/*--------------------------------- check results MaxSubjsObj -------------------------------------------*/
	@Test
	public void shouldExtractTriplesConceptThingMaxInverse() throws Exception{
		mockMvc.perform(get("/api/v1/groupedExtractor")
				.param("subj", dboMusicalArtist)
				.param("pred", dboChild)
				.param("obj", owlThing)
				.param("dataset", dataset)
				.param("ontology", ontology)
				.param("akpType", concept)
				.param("cardinalityType", maxInverse)
				.param("predictedCardinality", "1")
				.param("limit", "100")
				.param("offset", "0")
				.param("sort", "true")
				.param("showTriples", "true"))
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Artist", 0))) // check absence of supertype violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Guitarist", 0))) // check absence of subtype violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("_OK", 0))) // check absence of false violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"obj\":", 2))) // 2 obj => 2 results
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/MusicalArtist_KO_12_UndefinedChild_1\"", 1))) // obj1
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/MusicalArtist_KO_12_UndefinedChild_2\"", 1))) // obj2
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"" + dboChild + "\"", 2))) // pred
		//.andExpect(content().string(StringMatcher.containsStringNTimes("\"2\"", 2))) // nsubjs
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/MusicalArtist_KO_1\"", 2))) 
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/MusicalArtist_KO_2\"", 2))) 
		;
	}

	@Test
	public void shouldExtractTriplesConceptConceptMaxInverse() throws Exception{
		mockMvc.perform(get("/api/v1/groupedExtractor")
				.param("subj", dboMusicalArtist)
				.param("pred", dboChild)
				.param("obj", dboMusicalArtist)
				.param("dataset", dataset)
				.param("ontology", ontology)
				.param("akpType", concept)
				.param("cardinalityType", maxInverse)
				.param("predictedCardinality", "1")
				.param("limit", "100")
				.param("offset", "0")
				.param("sort", "true")
				.param("showTriples", "true"))
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Artist", 0))) // check absence of supertype violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Guitarist", 0))) // check absence of subtype violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("Undefined", 0))) // check absence of untyped violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("_OK", 0))) // check absence of false violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"obj\":", 2))) // 2 obj => 2 results
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/MusicalArtist_KO_12_Child_1\"", 1))) // obj1
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/MusicalArtist_KO_12_Child_2\"", 1))) // obj2
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"" + dboChild + "\"", 2))) // pred
		//.andExpect(content().string(StringMatcher.containsStringNTimes("\"2\"", 2))) // nsubjs
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/MusicalArtist_KO_1\"", 2))) 
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/MusicalArtist_KO_2\"", 2))) 
		;
	}

	@Test
	public void shouldExtractTriplesConceptDatatypeMaxInverse() throws Exception{
		mockMvc.perform(get("/api/v1/groupedExtractor")
				.param("subj", dboMusicalArtist)
				.param("pred", dboAge)
				.param("obj", xsdInteger)
				.param("dataset", dataset)
				.param("ontology", ontology)
				.param("akpType", datatype)
				.param("cardinalityType", maxInverse)
				.param("predictedCardinality", "1")
				.param("limit", "100")
				.param("offset", "0")
				.param("sort", "true")
				.param("showTriples", "true"))
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Artist", 0))) // check absence of supertype violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Guitarist", 0))) // check absence of subtype violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("Undefined", 0))) // check absence of untyped violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("_OK", 0))) // check absence of false violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"obj\":", 2))) // 2 obj => 2 results
		.andExpect(content().string(StringMatcher.containsStringNTimes("333", 1))) // obj1
		.andExpect(content().string(StringMatcher.containsStringNTimes("666", 1))) // obj2
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"" + dboAge + "\"", 2))) // pred
		//.andExpect(content().string(StringMatcher.containsStringNTimes("\"2\"", 2))) // nsubjs
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/MusicalArtist_KO_1\"", 2))) 
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/MusicalArtist_KO_2\"", 2))) 
		;
	}

	@Test
	public void shouldExtractTriplesConceptLiteralMaxInverse() throws Exception{
		mockMvc.perform(get("/api/v1/groupedExtractor")
				.param("subj", dboMusicalArtist)
				.param("pred", foafName)
				.param("obj", rdfsLiteral)
				.param("dataset", dataset)
				.param("ontology", ontology)
				.param("akpType", datatype)
				.param("cardinalityType", maxInverse)
				.param("predictedCardinality", "1")
				.param("limit", "100")
				.param("offset", "0")
				.param("sort", "true")
				.param("showTriples", "true"))
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Artist", 0))) // check absence of supertype violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Guitarist", 0))) // check absence of subtype violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("Undefined", 0))) // check absence of untyped violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("_OK", 0))) // check absence of false violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"obj\":", 2))) // 2 obj => 2 results
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"MusicalArtist_KO_12 Name_1@en\"", 1))) // obj1
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"MusicalArtist_KO_12 Name_2@en\"", 1))) // obj2
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"" + foafName + "\"", 2))) // pred
		//.andExpect(content().string(StringMatcher.containsStringNTimes("\"2\"", 2))) // nsubjs
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/MusicalArtist_KO_1\"", 2))) 
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/MusicalArtist_KO_2\"", 2))) 
		;
	}


	@Test
	public void shouldExtractTriplesThingThingMaxInverse() throws Exception{
		mockMvc.perform(get("/api/v1/groupedExtractor")
				.param("subj", owlThing)
				.param("pred", dboChild)
				.param("obj", owlThing)
				.param("dataset", dataset)
				.param("ontology", ontology)
				.param("akpType", concept)
				.param("cardinalityType", maxInverse)
				.param("predictedCardinality", "1")
				.param("limit", "100")
				.param("offset", "0")
				.param("sort", "true")
				.param("showTriples", "true"))
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Artist", 0))) // check absence of typed violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("/MusicalArtist", 0))) // check absence of typed violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Guitarist", 0))) // check absence of typed violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("_OK", 0))) // check absence of false violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"obj\":", 2))) // 2 obj => 2 results
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/UndefinedSubj_KO_12_UndefinedChild_1\"", 1))) // obj1
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/UndefinedSubj_KO_12_UndefinedChild_1\"", 1))) // obj2
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"" + dboChild + "\"", 2))) // pred
		//.andExpect(content().string(StringMatcher.containsStringNTimes("\"2\"", 2))) // nsubjs
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/UndefinedSubj_KO_1\"", 2))) 
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/UndefinedSubj_KO_2\"", 2))) 
		;
	}

	@Test
	public void shouldExtractTriplesThingConceptMaxInverse() throws Exception{
		mockMvc.perform(get("/api/v1/groupedExtractor")
				.param("subj", owlThing)
				.param("pred", dboChild)
				.param("obj", dboMusicalArtist)
				.param("dataset", dataset)
				.param("ontology", ontology)
				.param("akpType", concept)
				.param("cardinalityType", maxInverse)
				.param("predictedCardinality", "1")
				.param("limit", "100")
				.param("offset", "0")
				.param("sort", "true")
				.param("showTriples", "true"))
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Artist", 0))) // check absence of typed violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("/MusicalArtist", 0))) // check absence of typed violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Guitarist", 0))) // check absence of typed violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("_OK", 0))) // check absence of false violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"obj\":", 2))) // 2 obj => 2 results
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/UndefinedSubj_KO_12_Child_1\"", 1))) // obj1
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/UndefinedSubj_KO_12_Child_1\"", 1))) // obj2
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"" + dboChild + "\"", 2))) // pred
		//.andExpect(content().string(StringMatcher.containsStringNTimes("\"2\"", 2))) // nsubjs
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/UndefinedSubj_KO_1\"", 2))) 
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/UndefinedSubj_KO_2\"", 2))) 
		;
	}

	@Test
	public void shouldExtractTriplesThingDatatypeMaxInverse() throws Exception{
		mockMvc.perform(get("/api/v1/groupedExtractor")
				.param("subj", owlThing)
				.param("pred", dboAge)
				.param("obj", xsdInteger)
				.param("dataset", dataset)
				.param("ontology", ontology)
				.param("akpType", datatype)
				.param("cardinalityType", maxInverse)
				.param("predictedCardinality", "1")
				.param("limit", "100")
				.param("offset", "0")
				.param("sort", "true")
				.param("showTriples", "true"))
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Artist", 0))) // check absence of typed violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("/MusicalArtist", 0))) // check absence of typed violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Guitarist", 0))) // check absence of typed violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("_OK", 0))) // check absence of false violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"obj\":", 2))) // 2 obj => 2 results
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"333", 1))) // obj1
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"666", 1))) // obj2
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"" + dboAge + "\"", 2))) // pred
		//.andExpect(content().string(StringMatcher.containsStringNTimes("\"2\"", 2))) // nsubjs
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/UndefinedSubj_KO_1\"", 2))) 
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/UndefinedSubj_KO_2\"", 2)))
		;
	}

	@Test
	public void shouldExtractTriplesThingLiteralMaxInverse() throws Exception{
		mockMvc.perform(get("/api/v1/groupedExtractor")
				.param("subj", owlThing)
				.param("pred", foafName)
				.param("obj", rdfsLiteral)
				.param("dataset", dataset)
				.param("ontology", ontology)
				.param("akpType", datatype)
				.param("cardinalityType", maxInverse)
				.param("predictedCardinality", "1")
				.param("limit", "100")
				.param("offset", "0")
				.param("sort", "true")
				.param("showTriples", "true"))
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Artist", 0))) // check absence of typed violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("/MusicalArtist", 0))) // check absence of typed violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Guitarist", 0))) // check absence of typed violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("_OK", 0))) // check absence of false violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"obj\":", 2))) // 2 obj => 2 results
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"UndefinedSubj_KO_12 Name_1@en\"", 1))) // obj1
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"UndefinedSubj_KO_12 Name_2@en\"", 1))) // obj2
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"" + foafName + "\"", 2))) // pred
		//.andExpect(content().string(StringMatcher.containsStringNTimes("\"2\"", 2))) // nsubjs
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/UndefinedSubj_KO_1\"", 2))) 
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/UndefinedSubj_KO_2\"", 2)))
		;
	}

	/******************************************** !showTriples **************************************************/
	/*------------------------------------------- output fields -----------------------------------------------*/

	@Test
	public void shouldShowCountFieldsMaxDirect() throws Exception{
		mockMvc.perform(get("/api/v1/groupedExtractor")
				.param("subj", dboMusicalArtist)
				.param("pred", dboChild)
				.param("obj", owlThing)
				.param("dataset", dataset)
				.param("ontology", ontology)
				.param("akpType", concept)
				.param("cardinalityType", maxDirect)
				.param("predictedCardinality", "1")
				.param("limit", "100")
				.param("offset", "0")
				.param("sort", "true")
				.param("showTriples", "false"))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"head\":", 1)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"vars\":", 1)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"results\":", 1)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"bindings\":", 1)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"subj\":", 2)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"pred\":", 2)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"nobjs\":", 2)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"value\":", 6)));
	}

	@Test
	public void shouldShowFCountieldsMaxInverse() throws Exception{
		mockMvc.perform(get("/api/v1/groupedExtractor")
				.param("subj", dboMusicalArtist)
				.param("pred", dboChild)
				.param("obj", owlThing)
				.param("dataset", dataset)
				.param("ontology", ontology)
				.param("akpType", concept)
				.param("cardinalityType", maxInverse)
				.param("predictedCardinality", "1")
				.param("limit", "100")
				.param("offset", "0")
				.param("sort", "true")
				.param("showTriples", "false"))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"head\":", 1)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"vars\":", 1)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"results\":", 1)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"bindings\":", 1)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"obj\":", 2)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"pred\":", 2)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"nsubjs\":", 2)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"value\":", 6)));
	}


	/*--------------------------------- check results MaxSubjObjs -------------------------------------------*/
	@Test
	public void shouldExtractCountConceptThingMaxDirect() throws Exception{
		mockMvc.perform(get("/api/v1/groupedExtractor")
				.param("subj", dboMusicalArtist)
				.param("pred", dboChild)
				.param("obj", owlThing)
				.param("dataset", dataset)
				.param("ontology", ontology)
				.param("akpType", concept)
				.param("cardinalityType", maxDirect)
				.param("predictedCardinality", "1")
				.param("limit", "100")
				.param("offset", "0")
				.param("sort", "true")
				.param("showTriples", "false"))
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Artist", 0))) // check absence of supertype violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Guitarist", 0))) // check absence of subtype violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("_OK", 0))) // check absence of false violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"subj\":", 2))) // 2 subj => 2 results
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/MusicalArtist_KO_1\"", 1))) // subj1
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/MusicalArtist_KO_2\"", 1))) // subj2
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"" + dboChild + "\"", 2))) // pred
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"4\"", 2))) // nobjs
		;
	}

	@Test
	public void shouldExtractCountConceptConceptMaxDirect() throws Exception{
		mockMvc.perform(get("/api/v1/groupedExtractor")
				.param("subj", dboMusicalArtist)
				.param("pred", dboChild)
				.param("obj", dboMusicalArtist)
				.param("dataset", dataset)
				.param("ontology", ontology)
				.param("akpType", concept)
				.param("cardinalityType", maxDirect)
				.param("predictedCardinality", "1")
				.param("limit", "100")
				.param("offset", "0")
				.param("sort", "false")
				.param("showTriples", "false"))
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Artist", 0))) // check absence of supertype violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Guitarist", 0))) // check absence of subtype violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("Undefined", 0))) // check absence of untyped violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("_OK", 0))) // check absence of false violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"subj\":", 2))) // 2 subj => 2 results
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/MusicalArtist_KO_1\"", 1))) // subj1
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/MusicalArtist_KO_2\"", 1))) // subj2
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"" + dboChild + "\"", 2))) // pred
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"4\"", 2))) // nobjs
		;
	}

	@Test
	public void shouldExtractCountConceptDatatypeMaxDirect() throws Exception{
		mockMvc.perform(get("/api/v1/groupedExtractor")
				.param("subj", dboMusicalArtist)
				.param("pred", dboAge)
				.param("obj", xsdInteger)
				.param("dataset", dataset)
				.param("ontology", ontology)
				.param("akpType", datatype)
				.param("cardinalityType", maxDirect)
				.param("predictedCardinality", "1")
				.param("limit", "100")
				.param("offset", "0")
				.param("sort", "true")
				.param("showTriples", "false"))
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Artist", 0))) // check absence of supertype violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Guitarist", 0))) // check absence of subtype violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("Undefined", 0))) // check absence of untyped violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("_OK", 0))) // check absence of false violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"subj\":", 2))) // 2 subj => 2 results
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/MusicalArtist_KO_1\"", 1))) // subj1
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/MusicalArtist_KO_2\"", 1))) // subj2
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"" + dboAge + "\"", 2))) // pred
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"4\"", 2))) // nobjs
		;
	}

	@Test
	public void shouldExtractCountConceptLiteralMaxDirect() throws Exception{
		mockMvc.perform(get("/api/v1/groupedExtractor")
				.param("subj", dboMusicalArtist)
				.param("pred", foafName)
				.param("obj", rdfsLiteral)
				.param("dataset", dataset)
				.param("ontology", ontology)
				.param("akpType", datatype)
				.param("cardinalityType", maxDirect)
				.param("predictedCardinality", "1")
				.param("limit", "100")
				.param("offset", "0")
				.param("sort", "true")
				.param("showTriples", "false"))
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Artist", 0))) // check absence of supertype violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Guitarist", 0))) // check absence of subtype violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("Undefined", 0))) // check absence of untyped violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("_OK", 0))) // check absence of false violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"subj\":", 2))) // 2 subj => 2 results
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/MusicalArtist_KO_1\"", 1))) // subj1
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/MusicalArtist_KO_2\"", 1))) // subj2
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"" + foafName + "\"", 2))) // pred
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"4\"", 2))) // nobjs
		;
	}


	@Test
	public void shouldExtractCountThingThingMaxDirect() throws Exception{
		mockMvc.perform(get("/api/v1/groupedExtractor")
				.param("subj", owlThing)
				.param("pred", dboChild)
				.param("obj", owlThing)
				.param("dataset", dataset)
				.param("ontology", ontology)
				.param("akpType", concept)
				.param("cardinalityType", maxDirect)
				.param("predictedCardinality", "1")
				.param("limit", "100")
				.param("offset", "0")
				.param("sort", "true")
				.param("showTriples", "false"))
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Artist", 0))) // check absence of typed violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("/MusicalArtist", 0))) // check absence of typed violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Guitarist", 0))) // check absence of typed violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("_OK", 0))) // check absence of false violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"subj\":", 2))) // 2 subj => 2 results
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/UndefinedSubj_KO_1\"", 1))) // subj1
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/UndefinedSubj_KO_2\"", 1))) // subj2
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"" + dboChild + "\"", 2))) // pred
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"4\"", 2))) // nobjs
		;
	}

	@Test
	public void shouldExtractCountThingConceptMaxDirect() throws Exception{
		mockMvc.perform(get("/api/v1/groupedExtractor")
				.param("subj", owlThing)
				.param("pred", dboChild)
				.param("obj", dboMusicalArtist)
				.param("dataset", dataset)
				.param("ontology", ontology)
				.param("akpType", concept)
				.param("cardinalityType", maxDirect)
				.param("predictedCardinality", "1")
				.param("limit", "100")
				.param("offset", "0")
				.param("sort", "true")
				.param("showTriples", "false"))
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Artist", 0))) // check absence of typed violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("/MusicalArtist", 0))) // check absence of typed violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Guitarist", 0))) // check absence of typed violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("_OK", 0))) // check absence of false violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"subj\":", 2))) // 2 subj => 2 results
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/UndefinedSubj_KO_1\"", 1))) // subj1
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/UndefinedSubj_KO_2\"", 1))) // subj2
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"" + dboChild + "\"", 2))) // pred
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"4\"", 2))) // nobjs
		;
	}

	@Test
	public void shouldExtractCountThingDatatypeMaxDirect() throws Exception{
		mockMvc.perform(get("/api/v1/groupedExtractor")
				.param("subj", owlThing)
				.param("pred", dboAge)
				.param("obj", xsdInteger)
				.param("dataset", dataset)
				.param("ontology", ontology)
				.param("akpType", datatype)
				.param("cardinalityType", maxDirect)
				.param("predictedCardinality", "1")
				.param("limit", "100")
				.param("offset", "0")
				.param("sort", "true")
				.param("showTriples", "false"))
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Artist", 0))) // check absence of typed violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("/MusicalArtist", 0))) // check absence of typed violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Guitarist", 0))) // check absence of typed violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("_OK", 0))) // check absence of false violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"subj\":", 2))) // 2 subj => 2 results
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/UndefinedSubj_KO_1\"", 1))) // subj1
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/UndefinedSubj_KO_2\"", 1))) // subj2
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"" + dboAge + "\"", 2))) // pred
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"4\"", 2))) // nobjs
		;
	}

	@Test
	public void shouldExtractCountThingLiteralMaxDirect() throws Exception{
		mockMvc.perform(get("/api/v1/groupedExtractor")
				.param("subj", owlThing)
				.param("pred", foafName)
				.param("obj", rdfsLiteral)
				.param("dataset", dataset)
				.param("ontology", ontology)
				.param("akpType", datatype)
				.param("cardinalityType", maxDirect)
				.param("predictedCardinality", "1")
				.param("limit", "100")
				.param("offset", "0")
				.param("sort", "true")
				.param("showTriples", "false"))
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Artist", 0))) // check absence of typed violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("/MusicalArtist", 0))) // check absence of typed violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Guitarist", 0))) // check absence of typed violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("_OK", 0))) // check absence of false violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"subj\":", 2))) // 2 subj => 2 results
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/UndefinedSubj_KO_1\"", 1))) // subj1
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/UndefinedSubj_KO_2\"", 1))) // subj2
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"" + foafName + "\"", 2))) // pred
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"4\"", 2))) // nobjs
		;
	}

	/*--------------------------------- check results MaxSubjsObj -------------------------------------------*/
	@Test
	public void shouldExtractCountConceptThingMaxInverse() throws Exception{
		mockMvc.perform(get("/api/v1/groupedExtractor")
				.param("subj", dboMusicalArtist)
				.param("pred", dboChild)
				.param("obj", owlThing)
				.param("dataset", dataset)
				.param("ontology", ontology)
				.param("akpType", concept)
				.param("cardinalityType", maxInverse)
				.param("predictedCardinality", "1")
				.param("limit", "100")
				.param("offset", "0")
				.param("sort", "true")
				.param("showTriples", "false"))
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Artist", 0))) // check absence of supertype violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Guitarist", 0))) // check absence of subtype violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("_OK", 0))) // check absence of false violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"obj\":", 2))) // 2 obj => 2 results
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/MusicalArtist_KO_12_UndefinedChild_1\"", 1))) // obj1
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/MusicalArtist_KO_12_UndefinedChild_2\"", 1))) // obj2
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"" + dboChild + "\"", 2))) // pred
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"2\"", 2))) // nsubjs
		;
	}

	@Test
	public void shouldExtractCountConceptConceptMaxInverse() throws Exception{
		mockMvc.perform(get("/api/v1/groupedExtractor")
				.param("subj", dboMusicalArtist)
				.param("pred", dboChild)
				.param("obj", dboMusicalArtist)
				.param("dataset", dataset)
				.param("ontology", ontology)
				.param("akpType", concept)
				.param("cardinalityType", maxInverse)
				.param("predictedCardinality", "1")
				.param("limit", "100")
				.param("offset", "0")
				.param("sort", "true")
				.param("showTriples", "false"))
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Artist", 0))) // check absence of supertype violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Guitarist", 0))) // check absence of subtype violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("Undefined", 0))) // check absence of untyped violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("_OK", 0))) // check absence of false violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"obj\":", 2))) // 2 obj => 2 results
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/MusicalArtist_KO_12_Child_1\"", 1))) // obj1
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/MusicalArtist_KO_12_Child_2\"", 1))) // obj2
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"" + dboChild + "\"", 2))) // pred
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"2\"", 2))) // nsubjs
		;
	}

	@Test
	public void shouldExtractCountConceptDatatypeMaxInverse() throws Exception{
		mockMvc.perform(get("/api/v1/groupedExtractor")
				.param("subj", dboMusicalArtist)
				.param("pred", dboAge)
				.param("obj", xsdInteger)
				.param("dataset", dataset)
				.param("ontology", ontology)
				.param("akpType", datatype)
				.param("cardinalityType", maxInverse)
				.param("predictedCardinality", "1")
				.param("limit", "100")
				.param("offset", "0")
				.param("sort", "true")
				.param("showTriples", "false"))
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Artist", 0))) // check absence of supertype violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Guitarist", 0))) // check absence of subtype violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("Undefined", 0))) // check absence of untyped violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("_OK", 0))) // check absence of false violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"obj\":", 2))) // 2 obj => 2 results
		.andExpect(content().string(StringMatcher.containsStringNTimes("333", 1))) // obj1
		.andExpect(content().string(StringMatcher.containsStringNTimes("666", 1))) // obj2
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"" + dboAge + "\"", 2))) // pred
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"2\"", 2))) // nsubjs
		;
	}

	@Test
	public void shouldExtractCountConceptLiteralMaxInverse() throws Exception{
		mockMvc.perform(get("/api/v1/groupedExtractor")
				.param("subj", dboMusicalArtist)
				.param("pred", foafName)
				.param("obj", rdfsLiteral)
				.param("dataset", dataset)
				.param("ontology", ontology)
				.param("akpType", datatype)
				.param("cardinalityType", maxInverse)
				.param("predictedCardinality", "1")
				.param("limit", "100")
				.param("offset", "0")
				.param("sort", "true")
				.param("showTriples", "false"))
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Artist", 0))) // check absence of supertype violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Guitarist", 0))) // check absence of subtype violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("Undefined", 0))) // check absence of untyped violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("_OK", 0))) // check absence of false violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"obj\":", 2))) // 2 obj => 2 results
		.andExpect(content().string(StringMatcher.containsStringNTimes("MusicalArtist_KO_12 Name_1", 1))) // obj1
		.andExpect(content().string(StringMatcher.containsStringNTimes("MusicalArtist_KO_12 Name_2", 1))) // obj2
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"" + foafName + "\"", 2))) // pred
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"2\"", 2))) // nsubjs
		;
	}


	@Test
	public void shouldExtractCountThingThingMaxInverse() throws Exception{
		mockMvc.perform(get("/api/v1/groupedExtractor")
				.param("subj", owlThing)
				.param("pred", dboChild)
				.param("obj", owlThing)
				.param("dataset", dataset)
				.param("ontology", ontology)
				.param("akpType", concept)
				.param("cardinalityType", maxInverse)
				.param("predictedCardinality", "1")
				.param("limit", "100")
				.param("offset", "0")
				.param("sort", "true")
				.param("showTriples", "false"))
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Artist", 0))) // check absence of typed violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("/MusicalArtist", 0))) // check absence of typed violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Guitarist", 0))) // check absence of typed violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("_OK", 0))) // check absence of false violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"obj\":", 2))) // 2 obj => 2 results
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/UndefinedSubj_KO_12_UndefinedChild_1\"", 1))) // obj1
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/UndefinedSubj_KO_12_UndefinedChild_1\"", 1))) // obj2
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"" + dboChild + "\"", 2))) // pred
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"2\"", 2))) // nsubjs
		;
	}

	@Test
	public void shouldExtractCountThingConceptMaxInverse() throws Exception{
		mockMvc.perform(get("/api/v1/groupedExtractor")
				.param("subj", owlThing)
				.param("pred", dboChild)
				.param("obj", dboMusicalArtist)
				.param("dataset", dataset)
				.param("ontology", ontology)
				.param("akpType", concept)
				.param("cardinalityType", maxInverse)
				.param("predictedCardinality", "1")
				.param("limit", "100")
				.param("offset", "0")
				.param("sort", "true")
				.param("showTriples", "false"))
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Artist", 0))) // check absence of typed violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("/MusicalArtist", 0))) // check absence of typed violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Guitarist", 0))) // check absence of typed violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("_OK", 0))) // check absence of false violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"obj\":", 2))) // 2 obj => 2 results
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/UndefinedSubj_KO_12_Child_1\"", 1))) // obj1
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/UndefinedSubj_KO_12_Child_1\"", 1))) // obj2
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"" + dboChild + "\"", 2))) // pred
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"2\"", 2))) // nsubjs
		;
	}

	@Test
	public void shouldExtractCountThingDatatypeMaxInverse() throws Exception{
		mockMvc.perform(get("/api/v1/groupedExtractor")
				.param("subj", owlThing)
				.param("pred", dboAge)
				.param("obj", xsdInteger)
				.param("dataset", dataset)
				.param("ontology", ontology)
				.param("akpType", datatype)
				.param("cardinalityType", maxInverse)
				.param("predictedCardinality", "1")
				.param("limit", "100")
				.param("offset", "0")
				.param("sort", "true")
				.param("showTriples", "false"))
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Artist", 0))) // check absence of typed violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("/MusicalArtist", 0))) // check absence of typed violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Guitarist", 0))) // check absence of typed violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("_OK", 0))) // check absence of false violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"obj\":", 2))) // 2 obj => 2 results
		.andExpect(content().string(StringMatcher.containsStringNTimes("333", 1))) // obj1
		.andExpect(content().string(StringMatcher.containsStringNTimes("666", 1))) // obj2
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"" + dboAge + "\"", 2))) // pred
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"2\"", 2))) // nsubjs
		;
	}

	@Test
	public void shouldExtractCountThingLiteralMaxInverse() throws Exception{
		mockMvc.perform(get("/api/v1/groupedExtractor")
				.param("subj", owlThing)
				.param("pred", foafName)
				.param("obj", rdfsLiteral)
				.param("dataset", dataset)
				.param("ontology", ontology)
				.param("akpType", datatype)
				.param("cardinalityType", maxInverse)
				.param("predictedCardinality", "1")
				.param("limit", "100")
				.param("offset", "0")
				.param("sort", "true")
				.param("showTriples", "false"))
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Artist", 0))) // check absence of typed violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("/MusicalArtist", 0))) // check absence of typed violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Guitarist", 0))) // check absence of typed violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("_OK", 0))) // check absence of false violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"obj\":", 2))) // 2 obj => 2 results
		.andExpect(content().string(StringMatcher.containsStringNTimes("UndefinedSubj_KO_12 Name_1", 1))) // obj1
		.andExpect(content().string(StringMatcher.containsStringNTimes("UndefinedSubj_KO_12 Name_2", 1))) // obj2
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"" + foafName + "\"", 2))) // pred
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"2\"", 2))) // nsubjs
		;
	}

	/*--------------------------------- check sort / limit / offset -------------------------------------------*/

	@Test
	public void shouldSortMaxDirect() throws Exception {
		ArrayList<String> list = new ArrayList<String>();
		list.add("http://dbpedia.org/resource/Artist_SortLimitOffset_4");
		list.add("http://dbpedia.org/resource/Artist_SortLimitOffset_3");
		list.add("http://dbpedia.org/resource/Artist_SortLimitOffset_2");
		list.add("http://dbpedia.org/resource/Artist_SortLimitOffset_1");

		mockMvc.perform(get("/api/v1/groupedExtractor")
				.param("subj", dboArtist)
				.param("pred", dboAge)
				.param("obj", xsdInteger)
				.param("dataset", dataset)
				.param("ontology", ontology)
				.param("akpType", datatype)
				.param("cardinalityType", maxDirect)
				.param("predictedCardinality", "1")
				.param("limit", "100")
				.param("offset", "0")
				.param("sort", "true")
				.param("showTriples", "false"))
		.andExpect(content().string(stringContainsInOrder(list)));
		;
	}

	@Test
	public void shouldSortMaxInverse() throws Exception {
		ArrayList<String> list = new ArrayList<String>();
		list.add("11");
		list.add("12");
		list.add("13");
		list.add("14");
		
		
		mockMvc.perform(get("/api/v1/groupedExtractor")
				.param("subj", dboArtist)
				.param("pred", dboAge)
				.param("obj", xsdInteger)
				.param("dataset", dataset)
				.param("ontology", ontology)
				.param("akpType", datatype)
				.param("cardinalityType", maxInverse)
				.param("predictedCardinality", "1")
				.param("limit", "100")
				.param("offset", "0")
				.param("sort", "true")
				.param("showTriples", "false"))
		.andExpect(content().string(stringContainsInOrder(list)));

	}

	@Test
	public void shouldLimitMaxDirect() throws Exception {
		mockMvc.perform(get("/api/v1/groupedExtractor")
				.param("subj", dboArtist)
				.param("pred", dboAge)
				.param("obj", xsdInteger)
				.param("dataset", dataset)
				.param("ontology", ontology)
				.param("akpType", datatype)
				.param("cardinalityType", maxDirect)
				.param("predictedCardinality", "1")
				.param("limit", "2")
				.param("offset", "0")
				.param("sort", "false")
				.param("showTriples", "false"))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"subj\":", 2)))
		;
	}

	@Test
	public void shouldLimitMaxInverse() throws Exception {
		mockMvc.perform(get("/api/v1/groupedExtractor")
				.param("subj", dboArtist)
				.param("pred", dboAge)
				.param("obj", xsdInteger)
				.param("dataset", dataset)
				.param("ontology", ontology)
				.param("akpType", datatype)
				.param("cardinalityType", maxInverse)
				.param("predictedCardinality", "1")
				.param("limit", "2")
				.param("offset", "0")
				.param("sort", "false")
				.param("showTriples", "false"))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"obj\":", 2)))
		;
	}

	@Test
	public void shouldLimitOffsetMaxDirect() throws Exception {
		mockMvc.perform(get("/api/v1/groupedExtractor")
				.param("subj", dboArtist)
				.param("pred", dboAge)
				.param("obj", xsdInteger)
				.param("dataset", dataset)
				.param("ontology", ontology)
				.param("akpType", datatype)
				.param("cardinalityType", maxDirect)
				.param("predictedCardinality", "1")
				.param("limit", "2")
				.param("offset", "2")
				.param("sort", "false")
				.param("showTriples", "false"))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"subj\":", 1)))
		;
	}

	@Test
	public void shouldLimitOffsetMaxInverse() throws Exception {
		mockMvc.perform(get("/api/v1/groupedExtractor")
				.param("subj", dboArtist)
				.param("pred", dboAge)
				.param("obj", xsdInteger)
				.param("dataset", dataset)
				.param("ontology", ontology)
				.param("akpType", datatype)
				.param("cardinalityType", maxInverse)
				.param("predictedCardinality", "1")
				.param("limit", "2")
				.param("offset", "1")
				.param("sort", "false")
				.param("showTriples", "false"))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"obj\":", 2)))
		;
	}


}
