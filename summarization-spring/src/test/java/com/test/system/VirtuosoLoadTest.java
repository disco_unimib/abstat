package com.test.system;

import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.hp.hpl.jena.sparql.engine.http.QueryEngineHTTP;
import com.start.abstat.AbstatApplication;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = AbstatApplication.class)
@AutoConfigureMockMvc
public class VirtuosoLoadTest {
	@Autowired
	private MockMvc mockMvc;
	
	private String dataset;
	private String ontology;

	@Before
	public void init() throws Exception {
		dataset = "extraction-test";
		ontology = "dbpedia_2014";
		
		this.mockMvc.perform(post("/api/v1/summarizator")
        		.param("dataset", "extraction-test_dataset")
        		.param("ontologies", "extraction-test_ontology")
        		.param("cardinality", "true")
        		.param("shacl_validation", "true")
        		.param("async", "false"))
        .andDo(print()).andExpect(status().is2xxSuccessful());
	}

	@Test
	public void shouldHaveLoadedDataset() throws Exception {
		String sparql = "ASK WHERE { GRAPH <" + dataset + "> { ?s ?p ?o } }";
		QueryEngineHTTP request = new QueryEngineHTTP ("http://localhost:8890/sparql", sparql);
		assertTrue(request.execAsk());
		request.close();
	}
	
	@Test
	public void shouldHaveLoadedOntology() throws Exception {
		String sparql = "ASK WHERE { GRAPH <" + ontology + "> { ?s ?p ?o } }";
		QueryEngineHTTP request = new QueryEngineHTTP ("http://localhost:8890/sparql", sparql);
		assertTrue(request.execAsk());
		request.close();
	}
	
	
}
