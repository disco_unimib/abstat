package com.test.system;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.stringContainsInOrder;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.model.SubmitConfig;
import com.service.SubmitConfigService;
import com.start.abstat.AbstatApplication;



@RunWith(SpringRunner.class)
@SpringBootTest(classes = AbstatApplication.class)
@AutoConfigureMockMvc
public class BrowseAPITest {
	
    @Autowired
    private MockMvc mockMvc;
	@Autowired
	private SubmitConfigService service;
	 
	private String idSummary;
	  
    @Before
	public void init() {
    	List<SubmitConfig> list = service.listSubmitConfig();
		for(SubmitConfig config : list)
			if(config.getDsId().contentEquals("system-test_dataset") && 
					config.getListOntId().get(0).equals("system-test_ontology") && 
					config.isTipoMinimo() && 
					config.isInferences() && 
					config.isCardinalita() && 
					config.isPropertyMinimaliz()) 
				idSummary=config.getId();
	  }
	  
    @Test
    public void shouldWorkWithNoParams() throws Exception {
    	mockMvc.perform(get("/api/v1/browse")).andDo(print()).andExpect(status().isOk())
    	.andExpect(content().string(containsString("{ \"akps\": [ ")));
    }
    
    
    @Test
    public void shouldWork() throws Exception {
        mockMvc.perform(get("/api/v1/browse")
        		.param("summary", idSummary))
        .andDo(print()).andExpect(status().isOk())
        .andExpect(content().string(containsString("{ \"akps\": [ ")));
    }
    
    /*------------------------------------------- output fields -----------------------------------------------*/
    
    @Test
    public void shouldShowSubjectsPredicatesObjects() throws Exception{
    	 mockMvc.perform(get("/api/v1/browse")
    			 .param("summary", idSummary))
    	 .andExpect(content().string(StringMatcher.containsStringNTimes(" \"subject\" :", 178)))
    	 .andExpect(content().string(StringMatcher.containsStringNTimes(" \"predicate\" :", 178)))
    	 .andExpect(content().string(StringMatcher.containsStringNTimes(" \"object\" :", 178)));
    }
    
    @Test
    public void shouldShowAKPTypes() throws Exception{
    	 mockMvc.perform(get("/api/v1/browse")
    			 .param("summary", idSummary))
    	 .andExpect(content().string(StringMatcher.containsStringNTimes(" \"type\" :", 178)))
    	 .andExpect(content().string(StringMatcher.containsStringNTimes(" \"subType\" :", 178)));
    }
    
    @Test
    public void shouldShowMetadata() throws Exception{
    	 mockMvc.perform(get("/api/v1/browse")
    			 .param("summary", idSummary))
    	 .andExpect(content().string(StringMatcher.containsStringNTimes(" \"datasetOfOrigin\" :", 178)))
    	 .andExpect(content().string(StringMatcher.containsStringNTimes(" \"ontologiesOfOrigin\" :", 178)))
    	 .andExpect(content().string(StringMatcher.containsStringNTimes(" \"summary_conf\" :", 178)));
    }
    
    @Test
    public void shouldShowStatistics() throws Exception{
    	 mockMvc.perform(get("/api/v1/browse")
    			 .param("summary", idSummary))
    	 .andExpect(content().string(StringMatcher.containsStringNTimes(" \"frequency\" :", 178)))
    	 .andExpect(content().string(StringMatcher.containsStringNTimes(" \"numberOfInstances\" :", 178)))
    	 .andExpect(content().string(StringMatcher.containsStringNTimes(" \"cardinality1\" :", 178)))
    	 .andExpect(content().string(StringMatcher.containsStringNTimes(" \"cardinality2\" :", 178)))
    	 .andExpect(content().string(StringMatcher.containsStringNTimes(" \"cardinality3\" :", 178)))
    	 .andExpect(content().string(StringMatcher.containsStringNTimes(" \"cardinality4\" :", 178)))
    	 .andExpect(content().string(StringMatcher.containsStringNTimes(" \"cardinality5\" :", 178)))
    	 .andExpect(content().string(StringMatcher.containsStringNTimes("cardinality6\" :", 178)))
    	 .andExpect(content().string(StringMatcher.containsStringNTimes("cardinality6", 178)));
    }
    
    
    /*------------------------------------------- with constraints -----------------------------------------------*/
    
    @Test
    public void shouldWorkWithInternalSubject() throws Exception{
    	 mockMvc.perform(get("/api/v1/browse")
    			 .param("summary", idSummary)
    			 .param("subj", "http://schema.org/Place"))
    	 .andExpect(content().string(StringMatcher.containsStringNTimes("\"subject\" : \"http://schema.org/Place\",", 18)));
    }
    
    @Test
    public void shouldWorkWithExternalSubject() throws Exception{
    	 mockMvc.perform(get("/api/v1/browse")
    			 .param("summary", idSummary)
    			 .param("subj", "http://www.opengis.net/gml/_Feature"))
    	 .andExpect(content().string(StringMatcher.containsStringNTimes("\"subject\" : \"http://www.opengis.net/gml/_Feature\",", 9)));
    }
    
    @Test
    public void shouldWorkWithPredicate() throws Exception{
    	 mockMvc.perform(get("/api/v1/browse")
    			 .param("summary", idSummary)
    			 .param("pred", "http://xmlns.com/foaf/0.1/name"))
    	 .andExpect(content().string(StringMatcher.containsStringNTimes("\"predicate\" : \"http://xmlns.com/foaf/0.1/name\",", 22)));
    }
    
    @Test
    public void shouldWorkWithObject() throws Exception{
    	 mockMvc.perform(get("/api/v1/browse")
    			 .param("summary", idSummary)
    			 .param("obj", "http://www.w3.org/2001/XMLSchema#double"))
    	 .andExpect(content().string(StringMatcher.containsStringNTimes("\"object\" : \"http://www.w3.org/2001/XMLSchema#double\",", 26)));
    }
    
    @Test
    public void shouldWorkWithSubjectAndPredicate() throws Exception{
    	 mockMvc.perform(get("/api/v1/browse")
    			 .param("summary", idSummary)
    			 .param("subj", "http://schema.org/Place")
    			 .param("pred", "http://dbpedia.org/ontology/isPartOf"))
    	 .andExpect(content().string(containsString("\"object\" : \"http://dbpedia.org/ontology/AdministrativeRegion\"")))
    	 .andExpect(content().string(containsString("\"object\" : \"http://dbpedia.org/ontology/Wikidata:Q532\"")))
    	 .andExpect(content().string(containsString("\"object\" : \"http://schema.org/AdministrativeArea\"")))
    	 .andExpect(content().string(containsString("\"object\" : \"http://schema.org/Place\"")));
    }
    
    @Test
    public void shouldWorkWithSubjectAndObject() throws Exception{
    	 mockMvc.perform(get("/api/v1/browse")
    			 .param("summary", idSummary)
    			 .param("subj", "http://schema.org/Place")
    			 .param("obj", "http://www.w3.org/2001/XMLSchema#double"))
    	 .andExpect(content().string(containsString("  \"predicate\" : \"http://dbpedia.org/ontology/areaTotal\"")))
    	 .andExpect(content().string(containsString(" \"predicate\" : \"http://dbpedia.org/ontology/areaLand\"")))
    	 .andExpect(content().string(containsString("  \"predicate\" : \"http://dbpedia.org/ontology/areaWater\"")))
    	 .andExpect(content().string(containsString("  \"predicate\" : \"http://dbpedia.org/ontology/minimumElevation\"")))
    	 .andExpect(content().string(containsString("  \"predicate\" : \"http://dbpedia.org/ontology/maximumElevation\"")));
    }
    
    @Test
    public void shouldWorkWithpredicateAndObject() throws Exception{
    	 mockMvc.perform(get("/api/v1/browse")
    			 .param("summary", idSummary)
    			 .param("pred", "http://dbpedia.org/ontology/locationCity")
    			 .param("obj", "http://dbpedia.org/ontology/Place"))
    	 .andExpect(content().string(containsString("\"subject\" : \"http://dbpedia.org/ontology/TelevisionStation\"")));
    }
    
    /*--------------------- check SPO enrichment -----------------------------*/
    
    @Test
    public void should() throws Exception{
    	 mockMvc.perform(get("/api/v1/browse")
    			 .param("summary", idSummary)
    			 .param("subj", "http://schema.org/Place")
    			 .param("pred", "http://dbpedia.org/ontology/isPartOf")
    			 .param("enrichWithSPO", "true"))
    	 .andExpect(content().string(containsString("  \"subject\" : {\n" + 
    	 		"    \"globalURL\" : \"http://schema.org/Place\",\n" + 
    	 		"    \"frequency\" : 3")))
    	 .andExpect(content().string(containsString(" \"predicate\" : {\n" + 
    	 		"    \"globalURL\" : \"http://dbpedia.org/ontology/isPartOf\",\n" + 
    	 		"    \"frequency\" : 1")))
    	 .andExpect(content().string(containsString("\"object\" : {\n" + 
    	 		"    \"globalURL\" : \"http://dbpedia.org/ontology/AdministrativeRegion\",\n" + 
    	 		"    \"frequency\" : 1")))
    	 .andExpect(content().string(not(containsString("\"subject\" : \""))))
    	 .andExpect(content().string(not(containsString("\"predicate\" : \""))))
    	 .andExpect(content().string(not(containsString("\"object\" : \""))));
    }
    
    /*--------------------- check ranking -----------------------------*/
    
    @Test
    public void shouldRankInDescOrder() throws Exception{
		ArrayList<String> list = new ArrayList<String>();
		list.add(" \"frequency\" : 6,");
		list.add(" \"frequency\" : 3,");
		list.add(" \"frequency\" : 2,");
		list.add(" \"frequency\" : 2,");
		list.add(" \"frequency\" : 2,");
		list.add(" \"frequency\" : 1,");
		
    	mockMvc.perform(get("/api/v1/browse")
    			.param("summary", idSummary)
    			.param("subj", "http://schema.org/Place"))
    	 .andExpect(content().string(stringContainsInOrder(list)));
    }
    

    /*--------------------- check paging -----------------------------*/
    
    @Test
    public void shouldWorkWithValidLimit() throws Exception{
    	 mockMvc.perform(get("/api/v1/browse")
    			 .param("summary", idSummary)
    			 .param("subj", "http://schema.org/Place")
    			 .param("limit", "10"))
    	 .andExpect(content().string(StringMatcher.containsStringNTimes(" \"subject\" :", 10)));
    }
    
    @Test
    public void shouldWReturnAllResultsWithInvalidLimit() throws Exception{
    	 mockMvc.perform(get("/api/v1/browse")
    			 .param("summary", idSummary)
    			 .param("subj", "http://schema.org/Place")
    			 .param("limit", "-2"))
    	 .andExpect(content().string(StringMatcher.containsStringNTimes(" \"subject\" :", 18)));
    }
    
    @Test
    public void shouldWReturnAllResultsWithLimitZero() throws Exception{
    	 mockMvc.perform(get("/api/v1/browse")
    			 .param("summary", idSummary)
    			 .param("subj", "http://schema.org/Place")
    			 .param("limit", "0"))
    	 .andExpect(content().string(StringMatcher.containsStringNTimes(" \"subject\" :", 18)));
    }
    
    //----
    
    @Test
    public void shouldWorkWithValidOffset() throws Exception{
    	 mockMvc.perform(get("/api/v1/browse")
    			 .param("summary", idSummary)
    			 .param("subj", "http://schema.org/Place")
    			 .param("offset", "3"))
    	 .andExpect(content().string(not(containsString("dbfdba22fe2f2234f0b4ffea3eea86ae"))))
    	 .andExpect(content().string(not(containsString("ae87df565d5e8fd29ad9f809d8200648"))))
    	 .andExpect(content().string(not(containsString("82242e5e32bc2d53e51f9f3309c17717"))));
    }
    
    //----
    
    @Test
    public void pagingSholdWorkWithNormalParams1() throws Exception{
    	 mockMvc.perform(get("/api/v1/browse")
    			 .param("summary", idSummary)
    			 .param("subj", "http://schema.org/Place")
    			 .param("offset", "0")
    			 .param("limit", "3"))
    	 .andExpect(content().string(containsString("  \"predicate\" : \"http://xmlns.com/foaf/0.1/name\",\n" + 
    	 		"  \"object\" : \"http://www.w3.org/2000/01/rdf-schema#Literal\",")))
    	 .andExpect(content().string(containsString("  \"predicate\" : \"http://dbpedia.org/ontology/PopulatedPlace/areaTotal\",\n" + 
    	 		"  \"object\" : \"http://dbpedia.org/datatype/squareKilometre\",")))
    	 .andExpect(content().string(containsString("  \"predicate\" : \"http://dbpedia.org/ontology/areaWater\",\n" + 
    	 		"  \"object\" : \"http://www.w3.org/2001/XMLSchema#double\",")));
    }
    
    @Test
    public void pagingSholdWorkWithNormalParams2() throws Exception{
    	 mockMvc.perform(get("/api/v1/browse")
    			 .param("summary", idSummary)
    			 .param("subj", "http://schema.org/Place")
    			 .param("offset", "2")
    			 .param("limit", "3"))
    	 .andExpect(content().string(containsString("  \"predicate\" : \"http://dbpedia.org/ontology/areaTotal\",\n" + 
    	 		"  \"object\" : \"http://www.w3.org/2001/XMLSchema#double\",")))
    	 .andExpect(content().string(containsString("  \"predicate\" : \"http://dbpedia.org/ontology/areaLand\",\n" + 
    	 		"  \"object\" : \"http://www.w3.org/2001/XMLSchema#double\",")))
    	 .andExpect(content().string(containsString("  \"predicate\" : \"http://dbpedia.org/ontology/areaWater\",\n" + 
    	 		"  \"object\" : \"http://www.w3.org/2001/XMLSchema#double\",")));
    }
    
    @Test
    public void pagingSholdWorkWithOverflowParams() throws Exception{
    	 mockMvc.perform(get("/api/v1/browse")
    			 .param("summary", idSummary)
    			 .param("subj", "http://schema.org/Place")
    			 .param("offset", "16")
    			 .param("limit", "4"))
    	 .andExpect(content().string(containsString("  \"predicate\" : \"http://dbpedia.org/ontology/capital\",\n" + 
    	 		"  \"object\" : \"http://dbpedia.org/ontology/Wikidata:Q532\",")))
    	 .andExpect(content().string(containsString("\"predicate\" : \"http://dbpedia.org/ontology/capital\",\n" + 
    	 		"  \"object\" : \"http://dbpedia.org/ontology/Settlement\",")));
    }
    
    
    /*--------------------- check everything -----------------------------*/
    
    @Test
    public void shouldWorkEverything1() throws Exception{
    	 mockMvc.perform(get("/api/v1/browse")
    			 .param("summary", idSummary)
    			 .param("subj", "http://www.ontologydesignpatterns.org/ont/dul/DUL.owl#Agent")
    			 .param("pred", "http://dbpedia.org/ontology/birthPlace")
    			 .param("offset", "2")
    			 .param("limit", "10"))
    	 .andExpect(content().string(containsString("  \"object\" : \"http://dbpedia.org/ontology/AdministrativeRegion\"")))
    	 .andExpect(content().string(containsString("  \"object\" : \"http://schema.org/AdministrativeArea\"")))
    	 .andExpect(content().string(containsString(" \"object\" : \"http://dbpedia.org/ontology/Settlement\"")))
    	 .andExpect(content().string(containsString("  \"object\" : \"http://www.opengis.net/gml/_Feature\"")));
    }
    
    @Test
    public void shouldWorkEverything2() throws Exception{
		ArrayList<String> list = new ArrayList<String>();
		list.add("\"globalURL\" : \"http://dbpedia.org/datatype/squareKilometre\"");
		list.add("\"globalURL\" : \"http://www.w3.org/2001/XMLSchema#double\"");
		list.add("\"globalURL\" : \"http://www.w3.org/2001/XMLSchema#double\"");
		list.add("\"globalURL\" : \"http://www.w3.org/2001/XMLSchema#double\"");
		list.add("\"globalURL\" : \"http://www.w3.org/2001/XMLSchema#float\"");
		
    	 mockMvc.perform(get("/api/v1/browse")
    			 .param("summary", idSummary)
    			 .param("subj", "http://dbpedia.org/ontology/Wikidata:Q532")
    			 .param("enrichWithSPO", "true")
    			 .param("offset", "1")
    			 .param("limit", "5"))
    	 .andExpect(content().string(stringContainsInOrder(list)))
    	 .andExpect(content().string(not(containsString("\"subject\" : \""))))
    	 .andExpect(content().string(not(containsString("\"predicate\" : \""))))
    	 .andExpect(content().string(not(containsString("\"object\" : \""))))
    	 .andExpect(content().string(StringMatcher.containsStringNTimes("  \"subject\" : {\n    \"globalURL\" :",5)))
    	 .andExpect(content().string(StringMatcher.containsStringNTimes("  \"predicate\" : {\n    \"globalURL\" :",5)))
    	 .andExpect(content().string(StringMatcher.containsStringNTimes("  \"object\" : {\n    \"globalURL\" :",5)));
    }
    
    
}
