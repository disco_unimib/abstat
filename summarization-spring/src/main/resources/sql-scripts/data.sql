INSERT INTO app_role (id, role_name, description) VALUES (2, 'ADMIN_USER', 'Admin User - Has permission to perform admin tasks');

-- USER
INSERT INTO app_user (id, password, username) VALUES (2,'<SHA256 password>', 'admin');


INSERT INTO user_role(user_id, role_id) VALUES(2,2);
