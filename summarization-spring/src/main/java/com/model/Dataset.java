package com.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Dataset {
	
	@Id
	private String id;
	private String name;
	private String path;
	private String timestamp;
	private String type;
	private String server;
	boolean split;
	private long numberOfTriples;
	
	
	public Dataset() {
		super();
	}

	public Dataset(String id, String name, String path, boolean split, String timestamp, String server, String type) {
		super();
		this.id = id;
		this.name = name;
		this.path = path;
		this.split = split;
		this.timestamp = timestamp;
		this.server = server;
		this.type = type;
		
	}

	public String getId() { return id; }
	public void setId(String id) { this.id = id; }

	public String getName() { return name; }
	public void setName(String name) { this.name = name; }

	public String getPath() { return path; }
	public void setPath(String path) { this.path = path; }

	public String getTimestamp() { return timestamp; }
	public void setTimestamp(String timestamp) { this.timestamp = timestamp; }

	public boolean isSplit() { return split; }
	public void setSplit(boolean split) { this.split = split; }

	public String getServer() { return server; }
	public void setServer(String server) { this.server = server; }
	
	public String getType() { return type; }
	public void setType(String type) { this.type = type; }

	public long getNumberOfTriples() { return numberOfTriples; }
	public void setNumberOfTriples(long numberTriples) { this.numberOfTriples = numberTriples;}
	
	
}
