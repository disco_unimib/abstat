package com.service;

import com.model.SubmitConfig;

public interface LoadService {
	
	public void init(SubmitConfig config, String domain);
	
	public void loadResources(String path, String type) throws Exception;
	
	public void loadAKPs(String path, String type) throws Exception;

}
