package com.service;

import com.model.SubmitConfig;

public interface IndexService {

	public void init(SubmitConfig config, String domain);
	
	public void indexSummary(String path, String type) throws Exception;
	
	public void indexAKPsAutocomplete(String path, String type) throws Exception ;
	
}
