package com.service;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.ResourceDao;
import com.model.Resource;

@Service
public class ResourceServiceImpl implements ResourceService{


	@Autowired
	ResourceDao resourceDao;

	
	public void add(Resource resource) {
		resourceDao.add(resource);
	}

	public void add(Collection<Resource> collection) {
		resourceDao.add(collection);
	}
	
	
	public void update(Resource resource) {
		resourceDao.update(resource);	
	}

	
	public void delete(Resource resource) {
		resourceDao.delete(resource);
	}
	
	
	public void deletebySummary(String summary_id) {
		resourceDao.deletebySummary(summary_id);
	}
	
	
	public Resource getResourceFromSummary(String globalURI, String summary) {
		return resourceDao.getResourceFromSummary(globalURI, summary);
	}


	public List<Resource> getResources(String summary) {
		return resourceDao.getResources(summary);
	}
}
