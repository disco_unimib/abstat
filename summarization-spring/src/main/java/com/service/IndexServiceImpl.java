package com.service;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.AKPSolr;
import com.model.ResourceSolr;
import com.model.SubmitConfig;
import com.repository.AKPRepo;
import com.repository.ResourceRepo;
import com.summarization.ontology.RDFResource;
import com.summarization.ontology.TypeOf;

@Service
public class IndexServiceImpl implements IndexService {
	@Autowired
	AKPRepo akpRepo;
	@Autowired
	ResourceRepo resRepo;
	
	// note that this is a Service and so a bean. Therefore this variables life-time is the same of the Bean.
	static final int BufferSize = 1000000;
	HashMap<String, Long> datatypes;
	HashMap<String, Long> concepts;
	HashMap<String, Long> datatype_properties;
	HashMap<String, Long> object_properties;
	String countConcepts;
	String countDatatype;
	String countObjectProperties;
	String countDatatypeProperties;
	String summary_id;
	TypeOf typeOf;
	String dataset;
	

	public void init(SubmitConfig config, String domain) {
		this.datatypes = null;
		this.concepts = null;
		this.datatype_properties = null;
		this.object_properties = null;
		this.dataset = config.getDsName();
		this.typeOf = new TypeOf(domain);;
		this.summary_id =  config.getId();;
		
		this.countConcepts = config.getSummaryPath() + "/patterns/count-concepts.json";
		this.countDatatype = config.getSummaryPath() + "/patterns/count-datatype.json";
		this.countObjectProperties = config.getSummaryPath() + "/patterns/count-object-properties.json";
		this.countDatatypeProperties = config.getSummaryPath() + "/patterns/count-datatype-properties.json";
	}
	
	
	@Override
	public void indexSummary(String path, String type) throws Exception {
		ArrayList<ResourceSolr> buffer = new ArrayList<ResourceSolr>();
		JSONParser parser = new JSONParser();
		BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(path), StandardCharsets.UTF_8));
		String line = "";
		while ((line = br.readLine()) != null) {
			JSONObject json =  (JSONObject) parser.parse(line);
			ResourceSolr res = new ResourceSolr();
			res.setType(type);
			res.setDataset(dataset);
			res.setSummary(summary_id);
			if(type.contains("Akp")){
				String subject = (String) json.get("subjType");
				String subjectLocalName = new RDFResource(subject).localName();
				String property = (String) json.get("pred");
				String propertyLocalName = new RDFResource(property).localName();
				String object = (String) json.get("objType");
				String objectLocalName = new RDFResource(object).localName();
				Long occurrences = (Long) json.get("freq");
				String subtype = "";

				if(type.equals("datatypeAkp")) 
					subtype =  typeOf.datatypeAKP(subject);
				else
					subtype =  typeOf.objectAKP(subject, object);
				
				res.setURI(new String[]{subject, property, object});
				res.setSubtype(subtype);
				res.setFullTextSearchField(new String[]{subjectLocalName, propertyLocalName, objectLocalName});
				res.setOccurrence(occurrences);
			}
			else {
				String resource = (String) json.get("resource");
				String localName = new RDFResource(resource).localName();
				Long occurrences = (Long) json.get("occ");
				String subtype = typeOf.resource(resource);
				
				res.setURI(new String[]{resource});
				res.setSubtype(subtype);
				res.setFullTextSearchField(new String[]{localName});
				res.setOccurrence(occurrences);
			}
			if(buffer.size() >= BufferSize) {
				resRepo.save(buffer);
				buffer.clear();
			}
			buffer.add(res);
		}
		if(!buffer.isEmpty())
			resRepo.save(buffer);
		br.close();
	}

	
	@Override
	public void indexAKPsAutocomplete(String path, String type) throws Exception {
		if(this.datatypes==null && this.concepts==null) 
			frequenciesReader();
		
		ArrayList<AKPSolr> buffer = new ArrayList<AKPSolr>();
		JSONParser parser = new JSONParser();
		BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(path), StandardCharsets.UTF_8));
		String line = "";
		while ((line = br.readLine()) != null) {
			JSONObject json =  (JSONObject) parser.parse(line);
			String subject =  (String) json.get("subjType");
			String subjectLocalName = new RDFResource(subject).localName();
			String property =  (String) json.get("pred");
			String propertyLocalName = new RDFResource(property).localName();
			String object =  (String) json.get("objType");
			String objectLocalName = new RDFResource(object).localName();
			Long occurrences =  (Long) json.get("freq");
			String subtype = "";
			
			if(type.equals("datatypeAkp")) 
				subtype =  typeOf.datatypeAKP(subject);
			else 
				subtype =  typeOf.objectAKP(subject, object);

			
			AKPSolr akp = new AKPSolr();
			akp.setURI(new String[]{
					subject, property, object
			});
			akp.setType(type);
			akp.setDataset(dataset);
			akp.setSummary(summary_id);
			akp.setSubtype(subtype);
			akp.setFullTextSearchField(new String[]{
					subjectLocalName, propertyLocalName, objectLocalName
			});
			akp.setSubject(subject);
			akp.setPredicate(property);
			akp.setObject(object);
			akp.setSubject_ngram(subjectLocalName);
			akp.setPredicate_ngram(propertyLocalName);
			akp.setObject_ngram(objectLocalName);
			akp.setOCcurrence(occurrences);
			
			
			if(concepts.containsKey(subject))                     // questo if è necessario perchè i concetti esterni non sono elecnati in count-concepts.txt e quidni non sono nella collection concepts
				akp.setSubjectFreq(concepts.get(subject));
			else
				akp.setSubjectFreq(0);
			
			if(type.equals("datatypeAkp")){
				if(datatype_properties.containsKey(property))
					akp.setPredicateFreq(datatype_properties.get(property));
				else
					akp.setPredicateFreq(0);
				if(datatypes.containsKey(object))
					akp.setObjectFreq(datatypes.get(object));
				else
					akp.setObjectFreq(0);
			}
			else{
				if(object_properties.containsKey(property))
					akp.setPredicateFreq(object_properties.get(property));
				else
					akp.setPredicateFreq(0);
				if(concepts.containsKey(object))                     // questo if è necessario perchè i concetti esterni non sono elecnati in count-concepts.txt e quidni non sono nella collection concepts
					akp.setObjectFreq(concepts.get(object));
				else
					akp.setObjectFreq(0);
			}
			
			akp.setSubject_plus_dataset(subject + "_" + dataset);
			akp.setPredicate_plus_dataset(property + "_" + dataset);
			akp.setObject_plus_dataset(object + "_" + dataset);
			
			if(buffer.size() >= BufferSize) {
				akpRepo.save(buffer);
				buffer.clear();
			}
			buffer.add(akp);	
		}
		if(!buffer.isEmpty())
			akpRepo.save(buffer);
		br.close();
	}

	
	public void frequenciesReader() throws Exception {
			this.datatypes = getFreqs(countDatatype);
			this.concepts = getFreqs(countConcepts);
			this.datatype_properties = getFreqs(countDatatypeProperties);
			this.object_properties = getFreqs(countObjectProperties);
	}

	
	// Extracts frequencies from the input file
	public HashMap<String, Long> getFreqs(String path) throws Exception {
		HashMap<String, Long> map = new HashMap<String, Long>();
		JSONParser parser = new JSONParser();
		BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(path), StandardCharsets.UTF_8));
		String line = "";
		while ((line = br.readLine()) != null) {
			JSONObject json =  (JSONObject) parser.parse(line);
			String key = (String) json.get("resource");
			Long value = (Long) json.get("occ");
			map.put(key, value);	
		}
		br.close();
		return map;
	}

}
