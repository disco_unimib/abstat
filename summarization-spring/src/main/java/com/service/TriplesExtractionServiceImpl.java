package com.service;

import java.io.ByteArrayOutputStream;

import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.rdf.model.RDFNode;
import org.springframework.stereotype.Service;

import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.ResultSetFormatter;
import com.hp.hpl.jena.sparql.engine.http.QueryEngineHTTP;

@Service
public class TriplesExtractionServiceImpl implements TriplesExtractionService {


	public String extractGroupedTriples(String subj, String pred, String obj, String dataset, String ontology,
			String akpType, String cardinalityType, Integer predictedCardinality, Integer limit, Integer offset,
			boolean sort, boolean showTriples) {


		// preparing parameters
		subj = "<" + subj + ">";
		pred = "<" + pred + ">";
		obj = "<" + obj + ">";
		dataset = "<" + dataset + ">";
		ontology = "<" + ontology + ">";

		// preparing ?variables
		String grouperTerm;
		String countedTerm;
		switch(cardinalityType) {
		case "MaxSubjsObj":
			grouperTerm = "obj";
			countedTerm = "subj";
			break;
		case "MaxSubjObjs":
			grouperTerm = "subj";
			countedTerm = "obj";
			break;
		default:
			return null;
		} 


		// common part
		String sparql =	"select ?" + grouperTerm + " " + pred + " as ?pred " +
				"count(distinct(?" + countedTerm + ")) as ?n" + countedTerm + "s\n"; 

		// GROUP_CONCAT
		if(showTriples) {
			sparql += "group_concat(distinct(?" + countedTerm + "), '##') as ?" + countedTerm + "s \n";
		}

		// common part
		sparql += "from " + dataset + " \n " + 
				"from " + ontology + " \n ";

		sparql += "where { \n ";

		// different part SUBJECT
		if(subj.equalsIgnoreCase("<http://www.w3.org/2002/07/owl#Thing>")) {
			sparql += "   filter not exists {?subj a ?tsubj .} . \n ";
		}
		else {
			sparql += "   ?subj a " + subj +" . \n";
			sparql += "   filter not exists {?subj a ?tsubj . ?tsubj rdfs:subClassOf+ " + subj + " .} . \n ";
		}

		// common part
		sparql += "   ?subj " + pred + " ?obj . \n ";

		// different part OBJECT
		if(akpType.equalsIgnoreCase("DatatypeAKP")) {
			if(obj.equalsIgnoreCase("<http://www.w3.org/2000/01/rdf-schema#Literal>")) {
				sparql += "   filter(isLiteral(?obj)) . \n";
			}
			else {
				sparql += "   filter(datatype(?obj) = " + obj + ") . \n";
			}
		}
		else {
			if(obj.equalsIgnoreCase("<http://www.w3.org/2002/07/owl#Thing>")) {
				sparql += "   filter not exists {?obj a ?tobj .} . \n ";
			}
			else {
				sparql += "   ?obj a " + obj +" . \n";
				sparql += "   filter not exists {?obj a ?tobj . ?tobj rdfs:subClassOf+ " + obj + " .} . \n ";
			}

		}

		// common part
		sparql += "}\n"; 
		sparql += "GROUP BY ?" + grouperTerm + " \n" +  
				"HAVING (count(distinct(?" + countedTerm + ")) > " + predictedCardinality + ") \n ";

		// order by
		if(sort) {
			sparql += "ORDER BY DESC (?n" + countedTerm + "s)\n";
		}
		// limit
		if(limit != 0) {
			sparql += "LIMIT " + limit +"\n";
		}
		// offset pagination
		if(offset != 0) {
			sparql += "OFFSET " + offset*limit + "\n";
		}


		System.out.println(sparql);
		if(showTriples)
			return getCustomJSONTriples(sparql, grouperTerm, countedTerm, showTriples);
		else 
			return getJSONTriples(sparql);
	}





	@Override
	public String extractSingleTriples(String subj, String pred, String obj, String dataset, String ontology,
			String akpType, String cardinalityType, Integer limit, Integer offset) {
		// preparing parameters
		subj = "<" + subj + ">";
		pred = "<" + pred + ">";
		if(cardinalityType.equalsIgnoreCase("MaxSubjObjs") || (cardinalityType.equalsIgnoreCase("MaxSubjsObj") && !akpType.equalsIgnoreCase("DatatypeAKP"))) {
			obj = "<" + obj + ">";
		} else { // MaxSubjsObj datatype
			obj = "\"" + obj + "\"";
		}
		dataset = "<" + dataset + ">";
		ontology = "<" + ontology + ">";

		// preparing ?variables
		String filteredTerm;

		switch(cardinalityType) {
		case "MaxSubjsObj":
			filteredTerm = "subj";
			break;
		case "MaxSubjObjs":
			filteredTerm = "obj";
			break;
		default:
			return null;
		} 

		boolean filterSubj = filteredTerm.equalsIgnoreCase("subj");

		// common part
		String sparql =	"select ?" + filteredTerm + " " + pred + " as ?pred ";
		// different part STATIC TERM
		if(filterSubj) {
			sparql += obj + " as ?obj \n";
		}
		else {
			sparql += subj + " as ?subj \n";
		}
		// common part
		sparql += "from " + dataset + " \n " + 
				"from " + ontology + " \n " + 
				"where { \n ";

		// different part <SubjType, Prop, Obj> VS <Subj, Prop, ObjType>
		if(filterSubj) {
			if(akpType.equalsIgnoreCase("ObjectAKP")) {
				sparql += "   ?subj " + pred + " " + obj + " . \n ";
			}
			else {
				sparql += "   ?subj " + pred + " ?obj . \n ";
				sparql += "   filter(str(?obj) = " + obj + ") . \n";
			}

		}
		else {
			sparql += "   " + subj + " " + pred + " ?obj . \n ";
		}

		// different part FILTER
		if(filterSubj) {
			// SUBJECT
			if(subj.equalsIgnoreCase("<http://www.w3.org/2002/07/owl#Thing>")) {
				sparql += "   filter not exists {?subj a ?tsubj .} . \n ";
			}
			else {
				sparql += "   ?subj a " + subj +" . \n";
				sparql += "   filter not exists {?subj a ?tsubj . ?tsubj rdfs:subClassOf+ " + subj + " .} . \n ";
			}
		}
		else {
			// OBJECT
			if(akpType.equalsIgnoreCase("DatatypeAKP")) {
				if(obj.equalsIgnoreCase("<http://www.w3.org/2000/01/rdf-schema#Literal>")) {
					sparql += "   filter(isLiteral(?obj)) . \n";
				}
				else {
					sparql += "   filter(datatype(?obj) = " + obj + ") . \n";
				}
			}
			else {
				if(obj.equalsIgnoreCase("<http://www.w3.org/2002/07/owl#Thing>")) {
					sparql += "   filter not exists {?obj a ?tobj .} . \n ";
				}
				else {
					sparql += "   ?obj a " + obj +" . \n";
					sparql += "   filter not exists {?obj a ?tobj . ?tobj rdfs:subClassOf+ " + obj + " .} . \n ";
				}
			}
		}



		// common part
		sparql += "}  "; 

		// limit
		if(limit != 0) {
			sparql += "LIMIT " + limit +"\n";
		}

		// offset pagination
		if(offset != 0) {
			sparql += "OFFSET " + offset*limit;
		}
		System.out.println(sparql);
		return getJSONTriples(sparql);
	}

	private String getCustomJSONTriples(String sparql, String grouper, String counted, boolean showTriples) {

		QueryEngineHTTP request = new QueryEngineHTTP ("http://localhost:8890/sparql", sparql);
		ResultSet results = request.execSelect();

		String json = "{\n" +
				"  \"head\": {\n" + 
				"    \"vars\": [ \"" + grouper + "\" , \"pred\" , \"n" + counted + "s\"";

		if(showTriples) {
			json += ", \"" + counted + "s\"";
		}

		json += "    ]\n" +
				"  } ,\n" + 
				"  \"results\": {\n" + 
				"    \"bindings\": [\n" ;

		while (results.hasNext()) {
			QuerySolution result = results.nextSolution();

			json += "      {\n";
			json += "        \"" + grouper + "\": { \"value\": \"" + result.get(grouper) + "\"},\n";
			json += "        \"pred\": {\"value\": \"" + result.get("pred") + "\"},\n";
			json += "        \"n" + counted + "s\": {\"value\": \"" + result.getLiteral("n" + counted + "s").getInt() + "\" },\n";
			if(showTriples) {
				String values = "\"" + result.get(counted + "s").toString().replace("##", "\",\"") + "\"";				
				json += "        \"" + counted + "s\": {\"value\": [" + values + "] }\n";
			}
			json += "      }";
			if(results.hasNext())
				json += ",\n";
		}
		json += "    ]\n" + 
				"  }\n" + 
				"}";
		return json;

	}

	private String getJSONTriples(String sparql) {

		QueryEngineHTTP request = new QueryEngineHTTP ("http://localhost:8890/sparql", sparql);
		ResultSet results = request.execSelect();

		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		ResultSetFormatter.outputAsJSON(outputStream, results);
		String json = new String(outputStream.toByteArray());
		return json;

	}

}
