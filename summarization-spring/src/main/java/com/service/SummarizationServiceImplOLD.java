package com.service;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.model.Dataset;
import com.model.SubmitConfig;
import com.summarization.dataset.ParallelProcessing;
import com.summarization.dataset.ParallelSplitting;
import com.summarization.export.AggregateConceptCounts;
import com.summarization.export.CalculateMinimalTypes;
import com.summarization.export.CalculatePropertyMinimalization;
import com.summarization.export.DatatypeSplittedPatternInference;
import com.summarization.export.MainCardinality;
import com.summarization.export.ObjectSplittedPatternInference;
import com.summarization.export.ProcessDatatypeRelationAssertions;
import com.summarization.export.ProcessObjectRelationAssertions;
import com.summarization.export.ProcessOntology;

//@Service
//@Profile("single-machine")
public class SummarizationServiceImplOLD implements SummarizationService {

	private static final int nCores = Runtime.getRuntime().availableProcessors();
	@Autowired
	DatasetService datasetService;
	@Autowired
	OntologyService ontologyService;
	@Autowired
	SubmitConfigService submitConfigService;
	@Autowired
	EmailService emailService;
	
	
	@Async("processExecutor")
	public void summarizeAsyncWrapper(SubmitConfig subCfg, String email)   {
		summarize(subCfg, email);
	}


	public void summarize(SubmitConfig subCfg, String email) {
		try {
			Instant instant = Instant.now();
			
			String dsId = subCfg.getDsId();
			Dataset dataset = datasetService.findDatasetById(dsId);
			String datasetName = dataset.getName();
			String ontId = subCfg.getListOntId().get(0);
			String ontName = subCfg.getListOntNames().get(0);
			
			String datasetPath = new File(dataset.getPath()).getCanonicalPath();
			String ontPath = new File(ontologyService.findOntologyById(ontId).getPath()).getCanonicalPath();
			String summary_dir = new File(subCfg.getSummaryPath()).getCanonicalPath();
			String datasetSupportFileDirectory = summary_dir + "/reports/tmp-data-for-computation/";	
			String minTypeResult = summary_dir + "/min-types/min-type-results/";
			String patternsPath = summary_dir + "/patterns/";
			String shaclPath = patternsPath + "shacl/";
			String typesDirectory = "../data/DsAndOnt/dataset/" + datasetName + "/organized-splitted-deduplicated/";
			Path objectAkpGrezzo_target = Paths.get(patternsPath + "object-akp_grezzo.txt");
			Path datatypeAkpGrezzo_target = Paths.get(patternsPath + "datatype-akp_grezzo.txt");
			
			checkFile(summary_dir);
			checkFile(datasetSupportFileDirectory);
			checkFile(minTypeResult);
			checkFile(patternsPath);

			// Process ontology
			System.out.println("------------ ONTOLOGY PROCESSING");
			ProcessOntology.main(new String[] { ontPath, datasetSupportFileDirectory });			

			// Dataset splitting
			if(!dataset.isSplit()) {
				System.out.println("------------ DATA SET PREPROCESSING");
				String tempSplitDir = datasetPath.substring(0, datasetPath.lastIndexOf('/')) + "/organized-splitted-deduplicated-tmp-file";
				String splitDir = datasetPath.substring(0, datasetPath.lastIndexOf('/')) + "/organized-splitted-deduplicated";
				split(datasetPath, tempSplitDir, splitDir, dataset.getNumberOfTriples());
				dataset.setSplit(true);
				datasetService.update(dataset);
			}

			// core summarization
			coreSummarization(ontPath, typesDirectory, minTypeResult, patternsPath);

			// Property minimalization
			if (subCfg.isPropertyMinimaliz())
				propertyMinimalization(objectAkpGrezzo_target, datatypeAkpGrezzo_target, patternsPath, ontPath);
			// Inferenze
			if (subCfg.isInferences())
				inference(patternsPath, ontPath);
			// Cardinalità
			if (subCfg.isCardinalita())
				cardinality(patternsPath, subCfg.isRichCardinalities());
			// Shacl validation
			if(subCfg.isShaclValidation())
				shaclValidation(datasetPath, datasetName, ontPath, ontName, patternsPath, shaclPath);

			
			long numberPatterns = 0;
			LineNumberReader reader;
			if(new File(patternsPath + "/object-akp.txt").exists()){
				reader = new LineNumberReader(new FileReader(patternsPath + "/object-akp.txt"));
	    		while ((reader.readLine()) != null);
	    		numberPatterns += reader.getLineNumber();
	    		reader.close();
			}
			if(new File(patternsPath + "/datatype-akp.txt").exists()){
				reader = new LineNumberReader(new FileReader(patternsPath + "/datatype-akp.txt"));
	    		while ((reader.readLine()) != null);
	    		numberPatterns += reader.getLineNumber();
	    		reader.close();
			}
			
			subCfg.setNumberOfPatterns(numberPatterns);
			subCfg.setNumberOfTriples(dataset.getNumberOfTriples());
			subCfg.setServer("single-machine");
			
			// save configuration
			submitConfigService.add(subCfg);
			
			

			System.out.println("------------ COMPLETE. TOTAL time: " + Duration.between(instant, Instant.now()).getSeconds() +"s");
			
			// mail notification
			if(email != null  && !email.equals("") && !email.equals("undefined")) {
				emailService.sendMail(email, "OK");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			if(email != null  && !email.equals("")  && !email.equals("undefined")) {
				emailService.sendMail(email, "error");
			}
		}
	}	


	private void shaclValidation(String datasetPath, String datasetName, String ontologyPath, String ontologyName, String patternsPath, String shaclPath) throws IOException {
		System.out.println("------------ SHACL VALIDATION");

		// preparing bulk load parameters
		String datasetDirPath = datasetPath.substring(0, datasetPath.lastIndexOf("/"));
		String datasetFilename = datasetPath.replace(datasetDirPath + "/", "");
		String ontologyDirPath = ontologyPath.substring(0, ontologyPath.lastIndexOf("/"));
		String ontologyFilename = ontologyPath.replace(ontologyDirPath + "/", "");

		System.out.println("------------ Loading dataset in Virtuoso");
		runVirtuosoBulkLoad(datasetDirPath, datasetFilename, datasetName, true);

		System.out.println("------------ Loading ontology in Virtuoso");
		runVirtuosoBulkLoad(ontologyDirPath, ontologyFilename, ontologyName, true);

		System.out.println("------------ Predicting cardinalities");
		predictCardinalities(patternsPath);

	//	System.out.println("Generating shapes  ----------------------------------------------------------------");
	//	String shapesPath = shaclPath + "shapes";
		// ShaclGenerator.generateShacl(patternsPath, shapesPath);
	//	GenerateShacl.main(new String[] {patternsPath, shapesPath});

	//	System.out.println("Loading shapes in Virtuoso ----------------------------------------------------------------");
	//	runVirtuosoBulkLoad(shapesPath, "*.ttl", datasetName + "-shapes", false);
	}


	private void predictCardinalities(String patternsPath) throws IOException {
		String line; 
		String[] splittedLine;
		String akp;
		int maxSubjsObj;
		int avgSubjsObj;
		int maxSubjObjs;
		int avgSubjObjs;
		int maxSubjsObjPredicted;
		int maxSubjObjsPredicted;

		BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(patternsPath + "/patternCardinalities.txt"), StandardCharsets.UTF_8));
		Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(patternsPath + "/predictedCardinalities.txt"), StandardCharsets.UTF_8));

		while ((line = br.readLine()) != null) {
			try {
				splittedLine = line.split("##");
				akp = splittedLine[0] + "##" + splittedLine[1] + "##" + splittedLine[2];

				maxSubjsObj = Integer.parseInt(splittedLine[3]);
				avgSubjsObj = Integer.parseInt(splittedLine[4]);
				maxSubjObjs = Integer.parseInt(splittedLine[6]);
				avgSubjObjs = Integer.parseInt(splittedLine[7]);

				// prediction
				maxSubjsObjPredicted = maxSubjsObj > 2 * avgSubjsObj ? 2 * avgSubjsObj : maxSubjsObj;
				maxSubjObjsPredicted = maxSubjObjs > 2 * avgSubjObjs ? 2 * avgSubjObjs : maxSubjObjs;

				out.append(akp + "##" + maxSubjsObjPredicted + "##" + maxSubjObjsPredicted).append("\n");
				out.flush();
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println(line);
			}
		}

		br.close();
		out.close();
	}

	private void runVirtuosoBulkLoad(String datasetDirPath, String filenamePattern, String graphName, boolean smart) throws IOException {
		System.out.println("Running virtuoso bulk_load with parameters:");
		System.out.println("$1 = " + datasetDirPath);
		System.out.println("$2 = " + filenamePattern);
		System.out.println("$3 = " + graphName);
		
		String command = "bulk_load";
		if(smart)
			command = "smart_load_bulk";
		
		String script_path = new File(System.getProperty("user.dir")).getParent() + "/scripts/virtuoso.sh";
		ProcessBuilder pb = new ProcessBuilder( script_path, command, datasetDirPath, filenamePattern, graphName);
		Process p = pb.start();
		BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
		String line = "";
		while ((line = reader.readLine()) != null)
			System.out.println(line);
	}


	private void coreSummarization(String ontPath, String typesDirectory, String minTypeResult, String patternsPath) throws Exception {
		System.out.println("------------ MINIMAL TYPES CALCULATION");
		Instant instant = Instant.now();
		// Minimal types
		CalculateMinimalTypes.main(new String[] { ontPath, typesDirectory, minTypeResult });
		// Aggregate concepts
		AggregateConceptCounts.main(new String[] { minTypeResult, patternsPath });
		// Process dt relational asserts
		System.out.println(Duration.between(instant, Instant.now()).getSeconds() +"s" );
		System.out.println("------------ DATATYPE AKP CALCULATION");
		instant = Instant.now();
		ProcessDatatypeRelationAssertions.main(new String[] { typesDirectory, minTypeResult, patternsPath });
		// Process obj relational asserts
		System.out.println(Duration.between(instant, Instant.now()).getSeconds() +"s" );
		System.out.println("------------ OBJECT AKP CALCULATION");
		instant = Instant.now();
		ProcessObjectRelationAssertions.main(new String[] { typesDirectory, minTypeResult, patternsPath });
		System.out.println(Duration.between(instant, Instant.now()).getSeconds() +"s" );

		System.out.println("------------ merging grezzo files");
		instant = Instant.now();
		// sposto i file akp_grezzo
		Path objectAkpGrezzo_target = Paths.get(patternsPath + "object-akp_grezzo.txt");
		Path datatypeAkpGrezzo_target = Paths.get(patternsPath + "datatype-akp_grezzo.txt");
		Path datatypeAkpGrezzo_src = Paths.get("datatype-akp_grezzo.txt");
		Path objectAkpGrezzo_src = Paths.get("object-akp_grezzo.txt");

		mergeGrezzoFiles(new File("./"), "datatype");
		mergeGrezzoFiles(new File("./"), "object");
		
		if (datatypeAkpGrezzo_src.toFile().exists())
			Files.move(datatypeAkpGrezzo_src, datatypeAkpGrezzo_target, REPLACE_EXISTING);
		if (objectAkpGrezzo_src.toFile().exists())
			Files.move(objectAkpGrezzo_src, objectAkpGrezzo_target, REPLACE_EXISTING);
		
		System.out.println(Duration.between(instant, Instant.now()).getSeconds() +"s" );
	}
	
	
	private void mergeGrezzoFiles(File sourcePath, String type) throws Exception {
		for (File f : sourcePath.listFiles()) {
			if (f.getName().contains(type + "-akp_grezzo_")) {
				BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(f), StandardCharsets.UTF_8));
				BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(
						new FileOutputStream(sourcePath + "/" + type + "-akp_grezzo.txt", true), StandardCharsets.UTF_8));
				String line = "";
				while ((line = br.readLine()) != null)
					bw.write(line + "\n");
				br.close();
				bw.close();
				f.delete();
			}
		}
	}
	

	
	private void split(String datasetPath, String tempSplitDir, String splitDir, long numberTriples) throws Exception {
		checkFile(tempSplitDir);
		checkFile(splitDir);

		chunk(datasetPath, tempSplitDir, numberTriples);

		System.out.println("\nSTART parallel splitting");
		Instant nowSplit = Instant.now();
		ParallelSplitting splitterFasullo = new ParallelSplitting(new File(splitDir));
		new ParallelProcessing(new File(tempSplitDir)).splittingProcess(splitterFasullo, nCores);
		System.out.println(Duration.between(nowSplit, Instant.now()).getSeconds() +"s"  );

		System.out.println("\nSTART file merging");
		Instant nowMerging = Instant.now();
		mergeFiles(new File(splitDir));
		System.out.println(Duration.between(nowMerging, Instant.now()).getSeconds() + "s" );
		
	
		System.out.println("\nSTART sorting");
		Instant nowSort = Instant.now();
		for( File f : new File(splitDir).listFiles()) 
			sort(f);
		System.out.println(Duration.between(nowSort, Instant.now()).getSeconds() + "s" );
	
	}


	private void chunk(String datasetPath, String tempSplitDir, long numberTriples) throws Exception {
		Instant nowSort = Instant.now();
		System.out.println("START chunking");
		System.out.println("Number of triples: " + numberTriples);


		BufferedWriter[] writers = new BufferedWriter[nCores];
		for (int i = 0; i < nCores; i++)
			writers[i] = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(tempSplitDir + "/part_" + i,true), StandardCharsets.UTF_8));//;new BufferedWriter(new PrintWriter(tempSplitDir + "/part_" + i));

		BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(datasetPath), StandardCharsets.UTF_8));
		String line;
		long count = numberTriples / nCores;
		int i = 0;
		while ((line = br.readLine()) != null && !line.equals("")) {
			if (count == 0 && i < writers.length - 1) {
				writers[i].close();
				i++;
				count = numberTriples / nCores;
			}
			writers[i].write(line + "\n");
			count--;
		}
		writers[nCores - 1].close();
		br.close();

		System.out.println(Duration.between(nowSort, Instant.now()).getSeconds() + "s");
	}

	
	private void mergeFiles(File splitDir) throws Exception {
		for( File f : splitDir.listFiles()) {
			String prefix = "";
			String name = f.getName();
			if(!name.equals("error.txt")) 
				prefix = name.substring(0, name.lastIndexOf("_"));
			else
				prefix = "error";
			
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(f), StandardCharsets.UTF_8));
			BufferedWriter  bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(splitDir + "/" + prefix +".nt" ,true), StandardCharsets.UTF_8));
			String line = "";
			while ((line = br.readLine()) != null )//&& !line.equals("")) 
				bw.write(line + "\n");		
			br.close();
			bw.close();
			f.delete();		
		}
	}

	
	private void sort(File f) throws Exception {
		Instant now = Instant.now();
		String cmd = "sort -u " + f.getAbsolutePath() + " -o " + f.getAbsolutePath();
		System.out.println("sorting " + f.getName());
		Process p = Runtime.getRuntime().exec(cmd);
		BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
		String line = "";
		while ((line = reader.readLine()) != null)
			System.out.println(line);
		p.waitFor();

		System.out.println(f.getName() + ": " + Duration.between(now, Instant.now()).getSeconds() +"s");
	
	}
	

	private void propertyMinimalization(Path objectAkpGrezzo_target, Path datatypeAkpGrezzo_target, String patternsPath, String ontPath) throws Exception{
		System.out.println("------------ PROPERTY MINIMALIZATION");
		
		if(objectAkpGrezzo_target.toFile().exists()) 
			sort(objectAkpGrezzo_target.toFile());

		if(datatypeAkpGrezzo_target.toFile().exists()) 
			sort(datatypeAkpGrezzo_target.toFile());
		
		CalculatePropertyMinimalization.main(new String[] {ontPath, patternsPath});
		
		if(objectAkpGrezzo_target.toFile().exists()) {
			//sposto i file di output
			Path objectAkpGrezzo_Updated = Paths.get("object-akp_grezzo_Updated.txt");
			Files.move(objectAkpGrezzo_Updated, objectAkpGrezzo_target, REPLACE_EXISTING);
			Path objectAkp = Paths.get(patternsPath + "object-akp.txt");
			Path objectAkp_Updated = Paths.get("object-akp_Updated.txt");
			Files.move(objectAkp_Updated, objectAkp, REPLACE_EXISTING);
		}
		if(datatypeAkpGrezzo_target.toFile().exists()) {
			//sposto i file di output
			Path datatypeAkpGrezzo_Updated = Paths.get("datatype-akp_grezzo_Updated.txt");
			Files.move(datatypeAkpGrezzo_Updated, datatypeAkpGrezzo_target, REPLACE_EXISTING);
			Path datatypeAkp = Paths.get(patternsPath + "datatype-akp.txt");
			Path datatypeAkp_Updated = Paths.get("datatype-akp_Updated.txt");
			Files.move(datatypeAkp_Updated, datatypeAkp, REPLACE_EXISTING);
		}

	}


	private void inference(String patternsPath, String ontPath) throws Exception{
		checkFile(patternsPath + "AKPs_Grezzo-parts");
		checkFile(patternsPath+"specialParts_outputs");
		System.out.println("------------ DATATYPE INFERENCE");
		DatatypeSplittedPatternInference.main(new String[] {patternsPath, patternsPath + "AKPs_Grezzo-parts", ontPath, patternsPath+"specialParts_outputs"});
		System.out.println("------------ OBJECT INFERENCE");
		ObjectSplittedPatternInference.main(new String[] {patternsPath, patternsPath + "AKPs_Grezzo-parts", ontPath, patternsPath+"specialParts_outputs"});
	}

	private void cardinality(String patternsPath, boolean richCardinalities) throws Exception {
		if(richCardinalities)
			System.out.println("------------ CARDINALITY CALCULATION (RICH)");
		else
			System.out.println("------------ CARDINALITY CALCULATION");
		checkFile(patternsPath + "/Akps");
		checkFile(patternsPath + "/Properties");
		MainCardinality.main(new String[] { patternsPath, String.valueOf(richCardinalities) });
	}

	private void checkFile(String path_dir) throws Exception {
		File dir = new File(path_dir);
		if (dir.exists())
			FileUtils.deleteDirectory(dir);
		dir.mkdirs();
	}
}
