package com.service;


import java.util.Collection;
import java.util.List;

import com.model.Resource;

public interface ResourceService {
	
	public void add(Resource resource);
	
	public void add(Collection<Resource> collection);
	
	public void update(Resource resource);
	
	public void delete(Resource resource);
	
	public void deletebySummary(String summary_id);
	
	public Resource getResourceFromSummary(String globalURI, String summary);
	
	public List<Resource> getResources(String summary);
}
