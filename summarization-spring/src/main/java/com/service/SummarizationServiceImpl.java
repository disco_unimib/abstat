package com.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.model.Dataset;
import com.model.SubmitConfig;

@Service
@Profile("single-machine")
public class SummarizationServiceImpl  implements SummarizationService {

	@Autowired
	DatasetService datasetService;
	@Autowired
	OntologyService ontologyService;
	@Autowired
	SubmitConfigService submitConfigService;
	@Autowired
	EmailService emailService;

	
	@Async("processExecutor")
	public void summarizeAsyncWrapper(SubmitConfig subCfg, String email) {
		summarize(subCfg, email);
	}
	

	public void summarize(SubmitConfig subCfg, String email) {
		try {
			Dataset dataset = datasetService.findDatasetById(subCfg.getDsId());
			String datasetPath = new File(dataset.getPath()).getCanonicalPath();
			String datasetName = dataset.getName();
			String ontId = subCfg.getListOntId().get(0);
			String ontPath = new File(ontologyService.findOntologyById(ontId).getPath()).getCanonicalPath();
			String ontName = subCfg.getListOntNames().get(0);
			
			// run profiling pipeline
			sparkSubmit(subCfg);
			if(subCfg.isShaclValidation())
				shaclValidation(datasetPath, datasetName, ontPath, ontName);
			long numberPatterns = countPatterns(subCfg);

			subCfg.setNumberOfTriples(dataset.getNumberOfTriples());
			subCfg.setNumberOfPatterns(numberPatterns);
			subCfg.setServer("single-machine");
			submitConfigService.add(subCfg);
			emailService.sendMail(email, "OK");
			
		} catch (Exception e) {
			e.printStackTrace();
			emailService.sendMail(email, "error");
		}
	}
	
	
	private void sparkSubmit(SubmitConfig subCfg) throws Exception {
		String datasetPath = datasetService.findDatasetById(subCfg.getDsId()).getPath().replace("../",  "");
		String ontId = subCfg.getListOntId().get(0);
		String ontPath = ontologyService.findOntologyById(ontId).getPath().replace("../",  "");
		String output_dir = subCfg.getSummaryPath().replace("../",  "");
		output_dir += "/patterns/";
		String isDistributed = String.valueOf(false);
		String steps = String.valueOf(subCfg.isPropertyMinimaliz()) + " " + String.valueOf(subCfg.isInferences()) + " " + String.valueOf(subCfg.isCardinalita()) + " " + String.valueOf(subCfg.isShaclValidation());

		
		String[] cmd = { "/bin/bash", "abstat-distributed/submit-job.sh", datasetPath, ontPath, output_dir, isDistributed, steps};
		ProcessBuilder pb = new ProcessBuilder(cmd);
		pb.redirectErrorStream(true);
		pb.directory(new File(System.getProperty("user.dir")).getParentFile());
		Process p = pb.start();

		BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
		String line = "";
		boolean done = false;
		while ((line = reader.readLine()) != null) {
			System.out.println(line);
			if(line.contains("Summarization done"))
				done = true;
		}
		if(!done)
			throw new Exception("submit-job.sh exit with error");
	}

	

	private void shaclValidation(String datasetPath, String datasetName, String ontologyPath, String ontologyName) throws IOException {
		System.out.println("------------ SHACL VALIDATION");

		// preparing bulk load parameters
		String datasetDirPath = datasetPath.substring(0, datasetPath.lastIndexOf("/"));
		String datasetFilename = datasetPath.replace(datasetDirPath + "/", "");
		String ontologyDirPath = ontologyPath.substring(0, ontologyPath.lastIndexOf("/"));
		String ontologyFilename = ontologyPath.replace(ontologyDirPath + "/", "");

		System.out.println("------------ Loading dataset in Virtuoso");
		runVirtuosoBulkLoad(datasetDirPath, datasetFilename, datasetName, true);

		System.out.println("------------ Loading ontology in Virtuoso");
		runVirtuosoBulkLoad(ontologyDirPath, ontologyFilename, ontologyName, true);

	//	System.out.println("Generating shapes  ----------------------------------------------------------------");
	//	String shapesPath = shaclPath + "shapes";
		// ShaclGenerator.generateShacl(patternsPath, shapesPath);
	//	GenerateShacl.main(new String[] {patternsPath, shapesPath});

	//	System.out.println("Loading shapes in Virtuoso ----------------------------------------------------------------");
	//	runVirtuosoBulkLoad(shapesPath, "*.ttl", datasetName + "-shapes", false);
	}

	
	
	private void runVirtuosoBulkLoad(String datasetDirPath, String filenamePattern, String graphName, boolean smart) throws IOException {
		System.out.println("Running virtuoso bulk_load with parameters:");
		System.out.println("$1 = " + datasetDirPath);
		System.out.println("$2 = " + filenamePattern);
		System.out.println("$3 = " + graphName);
		
		String command = "bulk_load";
		if(smart)
			command = "smart_load_bulk";
		
		String script_path = new File(System.getProperty("user.dir")).getParent() + "/scripts/virtuoso.sh";
		ProcessBuilder pb = new ProcessBuilder( script_path, command, datasetDirPath, filenamePattern, graphName);
		Process p = pb.start();
		BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
		String line = "";
		while ((line = reader.readLine()) != null)
			System.out.println(line);
	}
	
	
	// count the number of patterns in a profile
	private long countPatterns(SubmitConfig subCfg) throws Exception {
		long numberPatterns = 0;
		LineNumberReader reader;
		String patternsPath = new File(subCfg.getSummaryPath()).getCanonicalPath() + "/patterns/";
		if(new File(patternsPath + "//object-akp-allstats.json").exists()){
			reader = new LineNumberReader(new FileReader(patternsPath + "/object-akp-allstats.json"));
    		while ((reader.readLine()) != null);
    		numberPatterns += reader.getLineNumber();
    		reader.close();
		}
		if(new File(patternsPath + "/datatype-akp-allstats.json").exists()){
			reader = new LineNumberReader(new FileReader(patternsPath + "/datatype-akp-allstats.json"));
    		while ((reader.readLine()) != null);
    		numberPatterns += reader.getLineNumber();
    		reader.close();
		}
		return numberPatterns;
	}
}
