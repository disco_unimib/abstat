package com.service;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;
import com.model.AKP;
import com.model.Resource;
import com.model.SubmitConfig;
import com.summarization.ontology.TypeOf;

@Service
public class LoadServiceImpl implements LoadService{
	@Autowired
	OntologyService ontologyService;
	@Autowired
	ResourceService resourceService;
	@Autowired
	AKPService AKPService;

	// note that this is a Service and so a bean. Therefore this variables life-time is the same of the Bean.
	static final int BufferSize = 1000000;
	TypeOf typeOf;
	String dataset;
	String summary_id;
	List<String> ontListNames;
	List<String> ontList;
	

	public void init(SubmitConfig config, String domain) {
		this.dataset = config.getDsName();
		this.typeOf = new TypeOf(domain);
		this.summary_id = config.getId();
		this.ontListNames = config.getListOntNames();
		this.ontList = config.getListOntId();
	}
	
	
	@Override
	public void loadResources(String path, String type) throws Exception{
		ArrayList<Resource> batch = new ArrayList<Resource>();
		int count = BufferSize;
		
		JSONParser parser = new JSONParser();
		BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(path), StandardCharsets.UTF_8));
		String line = "";
		while ((line = br.readLine()) != null) {
			JSONObject json =  (JSONObject) parser.parse(line);
			String res = (String) json.get("resource");
			String frequency =  String.valueOf( (Long)json.get("occ"));
			
			com.model.Resource resource = new com.model.Resource();
			resource.setId(DigestUtils.md5Hex(StringUtils.join(summary_id, res, type)));
			resource.setSeeAlso(res);
			resource.setType(type);
			resource.setSubType(typeOf.resource(res));
			resource.setDatasetOfOrigin(dataset);
			resource.setOntologiesOfOrigin(ontList);
			resource.setSummary_conf(summary_id);
			resource.setFrequency(Long.parseLong(frequency));
			
			if(count>0) {
				batch.add(resource);
				count--;
			}
			else {
				count = BufferSize;
				resourceService.add(batch);
				batch = new ArrayList<Resource>();
			}
		}
		br.close();
		resourceService.add(batch);
	}
	
	
	@Override	
	public void loadAKPs(String path, String type) throws Exception{
		ArrayList<AKP> batch = new ArrayList<AKP>();
		int count = BufferSize;
		
		Set<String> functionalProps = getFunctionalProps(false);
		Set<String> functionalInverseProps = getFunctionalProps(true);
		
		JSONParser parser = new JSONParser();
		BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(path), StandardCharsets.UTF_8));
		String line = "";
		while ((line = br.readLine()) != null) {
			JSONObject json =  (JSONObject) parser.parse(line);
			String subject = (String) json.get("subjType");
			String predicate = (String) json.get("pred");
			String object = (String) json.get("objType");

			AKP AKP = new AKP();
			String internal = null;
			if(type.equals("Object AKP"))
				internal = typeOf.objectAKP(subject, object);
			else if(type.equals("Datatype AKP"))
				internal = typeOf.datatypeAKP(subject);
			else
				internal = "NONE";

			AKP.setId(DigestUtils.md5Hex(StringUtils.join(summary_id, subject, predicate, object)));
			AKP.setPatternType("minimal");
			AKP.setType(type);
			AKP.setSubType(internal);
			AKP.setSubject(subject);
			AKP.setPredicate(predicate);
			AKP.setObject(object);
			AKP.setFrequency((Long) json.get("freq"));
			AKP.setDatasetOfOrigin(dataset);
			AKP.setOntologiesOfOrigin(ontList);
			AKP.setSummary_conf(summary_id);
			
			if(json.containsKey("numInstances"))
				AKP.setNumberOfInstances((Long) json.get("numInstances"));
			if(json.containsKey("d_min")) {
				AKP.setCardinality1((Long) json.get("d_max"));
				AKP.setCardinality2((Long) json.get("d_avg"));
				AKP.setCardinality3((Long) json.get("d_min"));
				AKP.setCardinality4((Long) json.get("i_max"));
				AKP.setCardinality5((Long) json.get("i_avg"));
				AKP.setCardinality6((Long) json.get("i_min"));
			}
			if(json.containsKey("d_predict")) {
				AKP.setPredictedCardinalityDirect((Long) json.get("d_predict"));
				AKP.setPredictedCardinalityInverse((Long) json.get("i_predict"));
				AKP = predictedCardinalities(AKP, functionalProps, functionalInverseProps);
			}
			else
				AKP.setStatus("not validated");
			
			if(count>0) {
				batch.add(AKP);
				count--;
			}
			else {
				batch.add(AKP);
				AKPService.add(batch);
				count = BufferSize;
				batch = new ArrayList<AKP>();
			}
		}
		br.close();
		AKPService.add(batch);
	}
	
	
	public AKP predictedCardinalities(AKP akp, Set<String> functionalProps, Set<String> functionalInverseProps) {
		String property = akp.getPredicate();
		boolean violatedSubjsObj = akp.getCardinality1() > akp.getPredictedCardinalityDirect();
		boolean violatedSubjObjs = akp.getCardinality4() > akp.getPredictedCardinalityInverse();
		
		// setting status
		String status = "";
		if(functionalProps.contains(property) || functionalInverseProps.contains(property)){
			if((functionalProps.contains(property) && akp.getCardinality4() > 1)  ||  (functionalInverseProps.contains(property) && akp.getCardinality1() > 1 )) 
				status = "invalid";	
			else 
				status = "valid";		
		}
		else {
			if(violatedSubjObjs || violatedSubjsObj)
				status = "warning";
			else
				status = "valid";
		}
		akp.setStatus(status);

		if(functionalInverseProps.contains(property)) 
			akp.setPredictedCardinalityDirect((long)1);
		if(functionalProps.contains(property)) 
			akp.setPredictedCardinalityInverse((long)1);
		return(akp);
	}
	
	
	public Set<String> getFunctionalProps(boolean isInverse){
		OntModel ontologyModel = ModelFactory.createOntologyModel(OntModelSpec.OWL_DL_MEM, null); 
		ontologyModel.read(ontologyService.findOntologyById(ontList.get(0)).getPath(), "RDF/XML");
		
		Set<String> props = new HashSet<String>();
		ExtendedIterator<OntProperty> it = ontologyModel.listAllOntProperties();
		while(it.hasNext()) {
			OntProperty prop = it.next();
			if((!isInverse && prop.isFunctionalProperty()) || (isInverse && prop.isInverseFunctionalProperty()))
				props.add(prop.getURI());
		}
		return props;
	}

}
