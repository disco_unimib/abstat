package com.dao;

import java.util.Collection;
import java.util.List;

import com.model.AKP;

public interface AKPDao {

	
	public void add(AKP AKP);
	
	public void add(Collection<AKP> collection);
	
	public void update(AKP AKP);
	
	public void delete(AKP AKP);
	
	public void deletebySummary(String summary_id);
	
	public List<AKP> list(String summary, String subj, String pred, String obj, Integer limit, Integer offset, String subtype);
	
	public List<String> getSPOlist(String summary, String position);
	
	public AKP getAKP(String subject, String predicate, String object, String summary);
	
	public List<AKP> getAKPs(String summary);
}
