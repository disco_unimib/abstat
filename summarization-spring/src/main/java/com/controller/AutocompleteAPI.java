package com.controller;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.summarization.export.QueryString;

@CrossOrigin(origins = "*")
@Controller
public class AutocompleteAPI {
	
	@Autowired
	HashMap<String, String> namespaces;
	
	
	@RequestMapping(value="/api/v1/SolrSuggestions", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getitem(@RequestParam Map<String,String> allRequestParams) throws Exception{ 
		String dataset = allRequestParams.get("dataset");
		String rows = allRequestParams.get("rows");
		String start = allRequestParams.get("start");

		if(start == null)
			start = "0";
		
		if(dataset==null) {
			return request(allRequestParams, null, rows, start);
		}
		else {
			String[] datasetList = dataset.split(",");
			ArrayList<JSONArray> responses = new ArrayList<JSONArray>();
			JSONObject response;
			int offset = Integer.valueOf(start);
			
			if(rows!=null) {
				int rowsLeft = Integer.valueOf(rows);

				for(int i=0; i<datasetList.length; i++) {
					if(rowsLeft>0) {				
						response = (JSONObject) new JSONParser().parse(request(allRequestParams, datasetList[i], String.valueOf(rowsLeft), String.valueOf(offset)));
						JSONArray arrayObj = (JSONArray)response.get("suggestions");
						responses.add(arrayObj);
						
						if(arrayObj.size()==0) {
							JSONObject tempObj  = (JSONObject) new JSONParser().parse(request(allRequestParams, datasetList[i], null, "0"));
							offset -= ( (JSONArray)tempObj.get("suggestions") ).size();
						}
						else {
							rowsLeft -= arrayObj.size();
							offset = 0;
						}
					}
				}
			}
		
			else {
				for(int i=0; i<datasetList.length; i++) {
					response = (JSONObject) new JSONParser().parse(request(allRequestParams, datasetList[i], null, String.valueOf(offset)));
					JSONArray arrayObj = (JSONArray)response.get("suggestions");
					responses.add(arrayObj);
					
					if(arrayObj.size()==0) {
						JSONObject tempObj  = (JSONObject) new JSONParser().parse(request(allRequestParams, datasetList[i], null, "0"));
						offset -= ( (JSONArray)tempObj.get("suggestions") ).size();
					}
					else {
						offset = 0;
					}
				}
			}
			
			
			JSONArray finalArray = new JSONArray();
			for(JSONArray jArray : responses) 
				finalArray.addAll(jArray);
		//	JSONObject result = new JSONObject();
		//	result.put("suggestions", finalArray);
			
		//	return result.toJSONString();
			return finalArray.toJSONString();
		}

	}
	
	
	private String request(Map<String, String> allRequestParams, String dataset, String rows, String start) throws Exception{
		String s = allRequestParams.get("subj");
		String p = allRequestParams.get("pred");
		String o = allRequestParams.get("obj");
		String group = allRequestParams.get("group");
		String qString = allRequestParams.get("qString");
		String qPosition = allRequestParams.get("qPosition");
		
		//	String q = query[0];
		int qLength = qString.length();
        QueryString queryString = new QueryString();
		String solrCore = "indexing";
		String suggestionService = "";
        
     // setting core
		if(s != null || p != null || o!= null)
			solrCore = "AKPs";
		
     // setting s,p,o costraints
		if(s != null){
			s = "\"" + s + "\"";
			queryString.addParameter("fq", "subject", s);
		}
		if(p != null){
			p = "\"" + p + "\"";
			queryString.addParameter("fq", "predicate", p);
		}
		if(o != null){
			o = "\"" + o + "\"";
			queryString.addParameter("fq", "object", o);
		}

	 // setting suggested service
        if(qPosition.equals("subj"))
			suggestionService = "concept-suggest";
		else if(qPosition.equals("pred"))
			suggestionService = "property-suggest";
		else if(qPosition.equals("obj"))
			suggestionService = "type-suggest";
        if(qLength < 3)
        	suggestionService += "-edge";

     // setting query parameter
        qString = qString.replace(" ", "%2520");   //because solr is queried using URL inputStream so whitespace codification is required
        queryString.addParameter("q", qString); 
		
        //setting dataset and paging parameters
		if(dataset!= null)
			queryString.addParameter("fq", "dataset", dataset);
		if(rows != null)
			queryString.addParameter("rows", rows);
		if(start != null)
			queryString.addParameter("start", start);
		
		
     // setting sort & group parameter
		if(solrCore.equals("indexing"))
			queryString.addParameter("sort", "occurrence desc");
        
		if(group != null && group.equals("true")){
		    queryString.addParameter("group", "true");
	        if(qPosition.equals("subj")){
	        	queryString.addParameter("sort", "subjectFreq desc");
	            queryString.addParameter("group.field", "subject_plus_dataset");
	        }
			else if(qPosition.equals("pred")){
	        	queryString.addParameter("sort", "predicateFreq desc");
				queryString.addParameter("group.field", "predicate_plus_dataset");
			}
			else if(qPosition.equals("obj")){
	        	queryString.addParameter("sort", "objectFreq desc");
				queryString.addParameter("group.field", "object_plus_dataset");
			}
		}
		
		
	     // JSON processing
			InputStream solrOutput = new URL("http://localhost:8983/solr/"+solrCore+"/" + suggestionService + queryString.build()).openStream();
		//	InputStream solrOutput =  connector.query("/solr/" + solrCore + "/" + suggestionService, queryString);
			String response; 
			if(group != null && group.equals("true"))
				response = extractSuggestionsAKPs(solrOutput, solrCore, qPosition);
			else 
				response = extractSuggestions(solrOutput, solrCore, qPosition);
		
			return response;
	}
	
	
	// Extracts suggestions and dataset  and return a String in JSON format
	private String extractSuggestions(InputStream inputStream, String core, String position) throws Exception {
	    //for output
	    JSONObject out = new JSONObject();
	    JSONArray suggestionList = new JSONArray();
		
	    //input parsing
		Object obj = new JSONParser().parse(IOUtils.toString(inputStream, StandardCharsets.UTF_8));
		JSONObject json = (JSONObject) obj;
	    JSONObject data = (JSONObject) json.get("response");
	    JSONArray docs = (JSONArray) data.get("docs");
	    
	    String URIString = "";
    	for(int i=0; i<docs.size(); i++){
	    	JSONObject doc = (JSONObject) docs.get(i);
    		JSONArray URI = (JSONArray)doc.get("URI");	
    		
    		JSONObject suggestion = new JSONObject();
	    	if(core.equals("indexing")){
			      suggestion.put("suggestion", URI.get(0).toString());
			      suggestion.put("occurrence", doc.get("occurrence"));
			      URIString = URI.get(0).toString();
	    	}
	    	else{
	    		if(position.equals("subj")){
				    suggestion.put("suggestion", URI.get(0).toString());
				    suggestion.put("occurrence", doc.get("subjectFreq"));
				    URIString = URI.get(0).toString();
	    		}
	    		else if(position.equals("pred")){
				    suggestion.put("suggestion", URI.get(1).toString());
				    suggestion.put("occurrence", doc.get("predicateFreq"));
				    URIString = URI.get(1).toString();
	    		}
	    		else {
				    suggestion.put("suggestion", URI.get(2).toString());
				    suggestion.put("occurrence", doc.get("objectFreq"));
				    URIString = URI.get(2).toString();
				    }
	    	}
	    	
	    	//retriving prefix from namespace
			Pattern pattern = Pattern.compile("(.*)[/#]");
			Matcher matcher = pattern.matcher(URIString);
			matcher.find();
			String namespace = matcher.group(1);
			String resto = URIString.replace(namespace, "");
	    	namespace += resto.charAt(0);
	    	
		      suggestion.put("dataset", doc.get("dataset"));
		      suggestion.put("namespace", namespace);
		      suggestion.put("prefix", namespaces.get(namespace));
		      suggestionList.add(suggestion);
    	}
    	
    	out.put("suggestions", suggestionList);
    	return out.toJSONString();
	}
		
		
	// Extracts suggestions and dataset  and return a String in JSON format
	private String extractSuggestionsAKPs(InputStream inputStream, String core, String position) throws Exception {
	    //for output
	    JSONObject out = new JSONObject();
	    JSONArray suggestionList = new JSONArray();
	    
	    //input parsing
		Object obj = new JSONParser().parse(IOUtils.toString(inputStream, StandardCharsets.UTF_8));
		JSONObject json = (JSONObject) obj;
		JSONObject plus_dataset;
	    JSONObject grouped = (JSONObject) json.get("grouped");
	    if(position.equals("subj"))
	    	plus_dataset = (JSONObject) grouped.get("subject_plus_dataset");
	    else if(position.equals("pred"))
	    	plus_dataset = (JSONObject) grouped.get("predicate_plus_dataset");
	    else
	    	plus_dataset = (JSONObject) grouped.get("object_plus_dataset");

	    JSONArray groups = (JSONArray) plus_dataset.get("groups");
	    
	    String URIString = "";
    	for(int i=0; i<groups.size(); i++){
	    	JSONObject group = (JSONObject) groups.get(i);
	    	JSONObject doclist = (JSONObject) group.get("doclist");
	    	JSONArray docs = (JSONArray) doclist.get("docs");
	    	JSONObject doc = (JSONObject) docs.get(0);
    		JSONArray URI = (JSONArray)doc.get("URI");	
    		
    		JSONObject suggestion = new JSONObject();
    		if(position.equals("subj")){
			    suggestion.put("suggestion", URI.get(0).toString());
			    suggestion.put("occurrence", doc.get("subjectFreq"));
			    URIString = URI.get(0).toString();
    		}
    		else if(position.equals("pred")){
			    suggestion.put("suggestion", URI.get(1).toString());
			    suggestion.put("occurrence", doc.get("predicateFreq"));
			    URIString = URI.get(1).toString();
    		}
    		else {
			    suggestion.put("suggestion", URI.get(2).toString());
			    suggestion.put("occurrence", doc.get("objectFreq"));
			    URIString = URI.get(2).toString();
			}
    		
	    	//retriving prefix from namespace
			Pattern pattern = Pattern.compile("(.*)[/#]");
			Matcher matcher = pattern.matcher(URIString);
			matcher.find();
			String namespace = matcher.group(1);
			String resto = URIString.replace(namespace, "");
			namespace += resto.charAt(0);
			
	        suggestion.put("dataset", doc.get("dataset"));
	     	suggestion.put("namespace", namespace);
		    suggestion.put("prefix", namespaces.get(namespace));
	        suggestionList.add(suggestion);
    	}
    	
    	out.put("suggestions", suggestionList);
    	return out.toJSONString();
	}

}
