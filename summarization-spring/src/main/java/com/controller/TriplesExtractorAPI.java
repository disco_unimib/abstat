package com.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.service.TriplesExtractionService;

@CrossOrigin(origins = "*")
@Controller
public class TriplesExtractorAPI {
	@Autowired
	TriplesExtractionService TriplesExtractionService;


	@RequestMapping(value="/api/v1/groupedExtractor", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String extractGroupedTriples(
			@RequestParam(value="subj", required=true) String subj, 
			@RequestParam(value="pred", required=true) String pred,
			@RequestParam(value="obj", required=true) String obj,
			@RequestParam(value="dataset", required=true) String dataset,
			@RequestParam(value="ontology", required=true) String ontology,
			@RequestParam(value="akpType", required=true) String akpType,
			@RequestParam(value="predictedCardinality", required=true) Integer predictedCardinality,
			@RequestParam(value="cardinalityType", required=true) String cardinalityType,
			@RequestParam(value="limit", required=false, defaultValue="0") Integer limit,
			@RequestParam(value="offset", required=false, defaultValue="0") Integer offset,
			@RequestParam(value="sort", required=false, defaultValue="false") Boolean sort,
			@RequestParam(value="showTriples", required=false, defaultValue="false") Boolean showTriples) {

		System.out.println("TriplesExtractionService");
		String results;

		results = TriplesExtractionService.extractGroupedTriples(subj, pred, obj, dataset, ontology, akpType, cardinalityType, predictedCardinality, limit, offset, sort, showTriples);


		return results;
	}
	
	@RequestMapping(value="/api/v1/singleExtractor", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String extractSingleTriples(
			@RequestParam(value="subj", required=true) String subj, 
			@RequestParam(value="pred", required=true) String pred,
			@RequestParam(value="obj", required=true) String obj,
			@RequestParam(value="dataset", required=true) String dataset,
			@RequestParam(value="ontology", required=true) String ontology,
			@RequestParam(value="akpType", required=true) String akpType,
			@RequestParam(value="cardinalityType", required=true) String cardinalityType,
			@RequestParam(value="limit", required=false, defaultValue="0") Integer limit,
			@RequestParam(value="offset", required=false, defaultValue="0") Integer offset) {

		System.out.println("TriplesExtractionService");
		String results;

		results = TriplesExtractionService.extractSingleTriples(subj, pred, obj, dataset, ontology, akpType, cardinalityType, limit, offset);


		return results;
	}
}
