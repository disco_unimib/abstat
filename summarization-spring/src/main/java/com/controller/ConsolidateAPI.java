package com.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.model.SubmitConfig;
import com.service.IndexService;
import com.service.LoadService;
import com.service.SubmitConfigService;

@CrossOrigin(origins = "*")
@Controller
public class ConsolidateAPI {
	
	@Autowired
	SubmitConfigService submitConfigService;
	@Autowired
	LoadService loadService;
	@Autowired
	IndexService indexService;
	//@Value("${spring.profiles.active}")
	//String profile;
	

	@RequestMapping(value = "/api/v1/consolidate", method = RequestMethod.POST)
	public String consolidate(@RequestParam(value="summary", required=true) String summary, 
			@RequestParam(value="store", required=false) Boolean store,
			@RequestParam(value="index", required=false) Boolean index,
			@RequestParam(value="domain", required=false) String domain) throws Exception{
		
		if(store==null) store=false;
		if(index==null) index=false;
	
		SubmitConfig config = submitConfigService.findSubmitConfigById(summary);
		String countConcepts = config.getSummaryPath() + "/patterns/count-concepts.json";
		String countDatatype = config.getSummaryPath() + "/patterns/count-datatype.json";
		String countObjectProperties = config.getSummaryPath() + "/patterns/count-object-properties.json";
		String countDatatypeProperties = config.getSummaryPath() + "/patterns/count-datatype-properties.json";
		String objectAKP = config.getSummaryPath() + "/patterns/object-akp-allstats.json";
		String datatypeAKP = config.getSummaryPath() + "/patterns/datatype-akp-allstats.json";
				
		if(store & !config.isLoadedMongoDB()) {
			loadService.init(config, domain);
			loadService.loadResources(countConcepts, "Concept");
			loadService.loadResources(countDatatype,  "Datatype");
			loadService.loadResources(countObjectProperties, "Object Property");
			loadService.loadResources(countDatatypeProperties, "Datatype Property");
			loadService.loadAKPs(objectAKP, "Object AKP");
			loadService.loadAKPs(datatypeAKP, "Datatype AKP");
		}
		if(index & !config.isIndexedSolr()) {
			indexService.init(config, domain);
			indexService.indexSummary(countConcepts, "concept");
			indexService.indexSummary(countDatatype,  "datatype");
			indexService.indexSummary(countObjectProperties, "objectProperty");
			indexService.indexSummary(countDatatypeProperties, "datatypeProperty");
			indexService.indexSummary(objectAKP, "objectAkp");
			indexService.indexSummary(datatypeAKP, "datatypeAkp");
			indexService.indexAKPsAutocomplete(objectAKP, "objectAkp");
			indexService.indexAKPsAutocomplete(datatypeAKP, "datatypeAkp");	
		}
		
		// update summary config 
		if(index) config.setIndexedSolr(index);
		if(store) config.setLoadedMongoDB(store);
		submitConfigService.update(config);
		return "redirect:home";
	}
	
}
