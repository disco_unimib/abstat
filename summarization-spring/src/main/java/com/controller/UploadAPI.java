package com.controller;

import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.LineNumberReader;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.model.Dataset;
import com.model.Ontology;
import com.service.DatasetService;
import com.service.FSCRUD;
import com.service.FileCRUD;
import com.service.OntologyService;

import org.apache.hadoop.hdfs.DistributedFileSystem;

@Controller
@CrossOrigin(origins = "*")
@RequestMapping(value = "/api/v1/upload")
public class UploadAPI {
	
	@Autowired
	DatasetService datasetService;
	@Autowired
	OntologyService ontologyService;
	@Autowired(required = false)
	DistributedFileSystem dfs;
	@Value("${spring.profiles.active}")
	String profile;
	@Value("${hdfs.address}")
	String address;
	@Value("${hdfs.port}")
	String port;
	@Value("${hdfs.abstatdir}")
	String abstatHDFSdir;
	FSCRUD fsCRUD = new FSCRUD();
	@Autowired
	FileCRUD fileCRUD;
	private final String FS_FOLDER = "../data/DsAndOnt/";

	
	@RequestMapping(value = "/ds", method = RequestMethod.POST)
	public String datasetUpload2(@RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes) throws Exception {
		if (file.isEmpty()) {
            redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
            return "redirect:status";
        }
		try {
			InputStream stream = file.getInputStream();
            Dataset dataset = new Dataset();
            dataset.setType("dataset");
            dataset.setName(file.getOriginalFilename().replaceFirst("[.][^.]+$", ""));
            SimpleDateFormat ft = new SimpleDateFormat ("dd/MM/yyyy hh:mm:ss");
            dataset.setTimestamp( ft.format(new Date()));
            String dir_path = FS_FOLDER + "dataset" + "/" + dataset.getName();
            dataset.setPath(fsCRUD.write(stream, dir_path  +"/"+ file.getOriginalFilename()));
            dataset.setServer("single-machine");
            
            System.out.println("counting dataset lines");
            LineNumberReader reader = new LineNumberReader(new FileReader(dataset.getPath()));
    		while ((reader.readLine()) != null);
    		int lines = reader.getLineNumber();
    		reader.close();
    		dataset.setNumberOfTriples(lines);
    		
			if (profile.equals("spark")) {
				String HDFSPath = fileCRUD.write(dataset.getPath(), address + ":" + port + abstatHDFSdir + "dataset/");
				dataset.setPath(HDFSPath);
				dataset.setServer("cluster");
			}
			
			datasetService.add(dataset);
			System.out.println("uploaded " + lines + " triples");
			redirectAttributes.addFlashAttribute("message", "You successfully uploaded '" + file.getName() + "'");    
        } 
		catch (IOException e) {
            e.printStackTrace();
        }
        return "redirect:status";
    }
	
	
	@RequestMapping(value = "/ont", method = RequestMethod.POST)
	public String ontologyUpload2(@RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes)  throws Exception{
		if (file.isEmpty()) {
            redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
            return "redirect:status";
        }
		try {
			InputStream stream = file.getInputStream();
            Ontology ontology = new Ontology();
            ontology.setType("ontology");
            ontology.setName(file.getOriginalFilename().replaceFirst("[.][^.]+$", ""));
	        SimpleDateFormat ft = new SimpleDateFormat ("dd/MM/yyyy hh:mm:ss");
            ontology.setTimestamp( ft.format(new Date()));
            String dir_path = FS_FOLDER + "ontology";
            ontology.setPath(fsCRUD.write(stream, dir_path  +"/"+ file.getOriginalFilename()));
            ontology.setServer("single-machine");
            
			if (profile.equals("spark")) {
				String HDFSPath = fileCRUD.write(ontology.getPath(), address + ":" + port + abstatHDFSdir + "ontology/");
				ontology.setPath(HDFSPath);
				ontology.setServer("cluster");
			}
			
        	ontologyService.add(ontology);
        	redirectAttributes.addFlashAttribute("message", "You successfully uploaded '" + file.getOriginalFilename() + "'");	
        } 
		catch (IOException e) {
            e.printStackTrace();
        }
        return "redirect:status";
    }
	
	
	@RequestMapping(value = "/status", method = RequestMethod.GET)
    public String uploadStatus() {
        return "operationStatus";
    }
	
}
