package com.controller;

import java.io.InputStream;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.summarization.export.QueryString;

@CrossOrigin(origins = "*")
@Controller
public class SPOAutocompleteAPI {

	@Autowired
	HashMap<String, String> namespaces;
	
	@RequestMapping(value="/api/v2/SPO", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String SPOAutocomplete(
			@RequestParam(value = "summary", required = false) String summary,
			@RequestParam(value = "qPosition", required = true) String qPosition,
			@RequestParam(value = "qString", required = true) String qString) throws Exception{
		
		int qLength = qString.length();
        QueryString queryString = new QueryString();
		String solrCore = "indexing";
		String suggestionService = "";

	 // setting suggested service
        if(qPosition.equals("subject"))
			suggestionService = "concept-suggest";
		else if(qPosition.equals("predicate"))
			suggestionService = "property-suggest";
		else if(qPosition.equals("object"))
			suggestionService = "type-suggest";
        if(qLength < 3)
        	suggestionService += "-edge";

     // setting query parameter
        qString = qString.replace(" ", "%2520");   //because solr is queried using URL inputStream so whitespace codification is required
        queryString.addParameter("q", qString); 
		
        //settingsummary
		if(summary!= null)
			queryString.addParameter("fq", "summary", summary);

     // setting sort
		queryString.addParameter("sort", "occurrence desc");

		InputStream solrOutput = new URL("http://localhost:8983/solr/"+solrCore+"/" + suggestionService + queryString.build()).openStream();
		String docs = format(solrOutput);
		return docs;
	}
	
	
	public String format(InputStream solrOutput) throws Exception{
	    //for output
	    JSONArray suggestionList = new JSONArray();
		
	    //input parsing
		Object obj = new JSONParser().parse(IOUtils.toString(solrOutput, StandardCharsets.UTF_8));
		JSONObject json = (JSONObject) obj;
	    JSONObject data = (JSONObject) json.get("response");
	    JSONArray docs = (JSONArray) data.get("docs");
	    
	    String URIString = "";
    	for(int i=0; i<docs.size(); i++){
	    	JSONObject doc = (JSONObject) docs.get(i);
    		JSONArray URI = (JSONArray)doc.get("URI");	
    		
    		JSONObject suggestion = new JSONObject();
			suggestion.put("suggestion", URI.get(0).toString());
			suggestion.put("occurrence", doc.get("occurrence"));
			URIString = URI.get(0).toString();
	    	
	    	//retriving prefix from namespace
			Pattern pattern = Pattern.compile("(.*)[/#]");
			Matcher matcher = pattern.matcher(URIString);
			matcher.find();
			String namespace = matcher.group(1);
			String resto = URIString.replace(namespace, "");
	    	namespace += resto.charAt(0);
	    
	    	String prefix = namespaces.get(namespace);
	    	if(prefix!=null)
	    		suggestion.put("prefixed", URI.get(0).toString().replace(namespace, prefix + ":"));  
	    	else
	    		suggestion.put("prefixed", URI.get(0).toString());  
		    suggestionList.add(suggestion);
    	}
		return suggestionList.toJSONString();
	}
	
}
