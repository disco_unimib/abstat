package com.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.model.SubmitConfig;
import com.repository.AKPRepo;
import com.repository.ResourceRepo;
import com.service.AKPService;
import com.service.FileCRUD;
import com.service.ResourceService;
import com.service.SubmitConfigService;

@CrossOrigin(origins = "*")
@Controller
public class SummaryCRUDAPI {

	@Autowired
	SubmitConfigService submitConfigService;
	@Autowired
	AKPRepo akpRepo;
	@Autowired
	ResourceRepo resRepo;
	@Autowired 
	ResourceService resourceService;
	@Autowired 
	AKPService AKPservice;
	@Value("${spring.profiles.active}")
	String profile;
	@Autowired
	FileCRUD fileCRUD;
	
	
	@RequestMapping(value = "/api/v1/submitconfig/delete/{id}", method = RequestMethod.POST)
		public String delete(@PathVariable("id") String id) throws IOException {
			SubmitConfig submitConfig = submitConfigService.findSubmitConfigById(id);
			
			akpRepo.deleteBySummary(submitConfig.getId());
			resRepo.deleteBySummary(submitConfig.getId());
			
			AKPservice.deletebySummary(submitConfig.getId());
			resourceService.deletebySummary(submitConfig.getId());
			
			submitConfigService.delete(submitConfig);
			
			if(submitConfig.isShaclValidation()) {
				deleteVirtuoso(submitConfig.getDsName(), submitConfig.getListOntNames().get(0));
			}
			
			
			return "redirect:/management";
	}
	
	
	@RequestMapping(value = "/api/v1/submitconfig/deleteDir/{id}", method = RequestMethod.POST)
	public String deleteDir(@PathVariable("id") String id) throws IOException {
		SubmitConfig submitConfig = submitConfigService.findSubmitConfigById(id);
		
		akpRepo.deleteBySummary(submitConfig.getId());
		resRepo.deleteBySummary(submitConfig.getId());
		
		AKPservice.deletebySummary(submitConfig.getId());
		resourceService.deletebySummary(submitConfig.getId());
		
		if(submitConfig.isShaclValidation()) 
			deleteVirtuoso(submitConfig.getDsName(), submitConfig.getListOntNames().get(0));

		submitConfigService.delete(submitConfig);
		
		if (profile.equals("single-machine"))
			fileCRUD.delete(StringUtils.substringBeforeLast(submitConfig.getSummaryPath(), "/"));
		else
			fileCRUD.delete(submitConfig.getSummaryPath());
		
		return "redirect:/management";
	}
	
	
	@RequestMapping(value="/api/v1/summaries", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getitem( @RequestParam(value="loaded", required=false) Boolean loaded,
										 @RequestParam(value="indexed", required=false) Boolean indexed,
										 @RequestParam(value="search", required=false) String search) {
		String summaries = submitConfigService.listSubmitConfigJSON(loaded, indexed, search);
		return summaries;		
	}
	
	
	private void deleteVirtuoso(String dataset, String ontology) throws IOException {
		// dataset
		deleteVirtuosoGraph(dataset);
		// ontology
		deleteVirtuosoGraph(ontology);
		// shapes
		deleteVirtuosoGraph(dataset + "-shapes");
	}
	
	
	private void deleteVirtuosoGraph(String graph) throws IOException {
		System.out.println("Running virtuoso delete_graph with parameters:");
		System.out.println("$1 = " + graph);
		ProcessBuilder pb = new ProcessBuilder("./../scripts/virtuoso.sh", "delete_graph", graph);
		Process p = pb.start();
		BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
		String line = "";
		while ((line = reader.readLine()) != null)
			System.out.println(line);
	}
	
	
	
	
}
