package com.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.model.Ontology;
import com.service.FileCRUD;
import com.service.OntologyService;

@CrossOrigin(origins = "*")
@Controller
public class OntologyCRUDAPI {

	@Autowired
	OntologyService ontologyService;
	@Autowired
	FileCRUD fileCRUD;

	@RequestMapping(value = "/api/v1/ontology/delete/{id}", method = RequestMethod.POST)
		public String delete(@PathVariable("id") String id) throws IOException {
			Ontology ontology = ontologyService.findOntologyById(id);
			ontologyService.delete(ontology);
			return "redirect:/management";
	}
	

	@RequestMapping(value = "/api/v1/ontology/deleteDir/{id}", method = RequestMethod.POST)
	public String deleteDir(@PathVariable("id") String id) throws IOException {
		Ontology ontology = ontologyService.findOntologyById(id);
		ontologyService.delete(ontology);

		//same if FS or HDFS
		fileCRUD.delete(ontology.getPath());
		
		return "redirect:/management";
	}
	
	
	@RequestMapping(value="/api/v1/ontologies", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getitem() {
		String datasets = ontologyService.listOntologyJSON();
		return datasets;
	}
}
