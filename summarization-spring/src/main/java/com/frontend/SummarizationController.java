package com.frontend;


import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.model.SubmitConfig;
import com.service.DatasetService;
import com.service.OntologyService;


@CrossOrigin(origins = "*")
@Controller
@RequestMapping(value = "summarize")
@SessionAttributes("submitConfig")
public class SummarizationController {

	
	@Autowired
	DatasetService datasetService;
	@Autowired
	OntologyService ontologyService;

	@Value("${server.port}")
	String serverPort;
	
/*
	@RequestMapping(value = "", method = RequestMethod.GET)
	public ModelAndView summarization() {
		ModelAndView model = new ModelAndView("summarize");
		model.addObject("listDataset", datasetService.listDataset());
		model.addObject("listOntology", ontologyService.listOntology());
		model.addObject("submitConfig", new SubmitConfig());
		return model;
	}
	

	@RequestMapping(value = "/recap", method = RequestMethod.POST)
	public ModelAndView submitCfg(@ModelAttribute("submitConfig") SubmitConfig config) throws Exception {
		//add new attributes to config
		String datasetName =  datasetService.findDatasetById(config.getDsId()).getName();
		ArrayList<String> ontlogiesListName = new ArrayList<String>();
		for(String id : config.getListOntId())
			ontlogiesListName.add(ontologyService.findOntologyById(id).getName());
		
        config.setDsName(datasetName);
        config.setListOntNames(ontlogiesListName);
		
		ModelAndView model = new ModelAndView("recapConfig");
		model.addObject("submitConfig", config);
				
		return model;
	}
	
	
	@RequestMapping(value = "/run", method = RequestMethod.POST)
	public String SummrizatorAPI(@ModelAttribute("submitConfig") SubmitConfig config,
			@RequestParam(value="email", required=false) String email,
			RedirectAttributes redirectAttributes){
		
		String ontologies = "";
		for(String ontology : config.getListOntId())
			ontologies += ontology + ",";
		ontologies = ontologies.substring(0, ontologies.length()-1);
		
		RestTemplate rest = new RestTemplate();
		MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
		map.add("dataset", config.getDsId());
		map.add("ontologies", ontologies);
		map.add("concept_min", String.valueOf(config.isTipoMinimo()));
		map.add("inference", String.valueOf(config.isInferences()));
		map.add("cardinality", String.valueOf(config.isCardinalita()));
		map.add("property_min", String.valueOf(config.isPropertyMinimaliz()));
		map.add("rich_cardinalities", String.valueOf(config.isRichCardinalities()));
		map.add("email", email);
		rest.postForObject("http://localhost:"+serverPort +"/summarizator", map, String.class);
		redirectAttributes.addFlashAttribute("message", "You successfully submit the summarization request");
		return "redirect:status";
	}
	
	@CrossOrigin(origins = {"http://localhost", "http://localhost:4200"})
	@RequestMapping(value = "/status", method = RequestMethod.GET)
    public String uploadStatus() {
        return "operationStatus";
    }
	*/
}
