package com.summarization.export;

import java.io.File;
import java.time.Duration;
import java.time.Instant;

import com.summarization.experiments.PropertyMinimalizator;
public class CalculatePropertyMinimalization {
	
	public static void main(String[] args) throws Exception{
		File ontology = new File(args[0]);
		String patterns_DirPath = args[1];
		
		File dt_grezzo = new File(patterns_DirPath + "/datatype-akp_grezzo.txt");
		File obj_grezzo = new File(patterns_DirPath + "/object-akp_grezzo.txt");
		
		System.out.println("START datatype property minimalization");
		Instant instant = Instant.now();
		if(dt_grezzo.exists()) {
			PropertyMinimalizator onDatatype= new PropertyMinimalizator(dt_grezzo,
					new File("datatype-akp_grezzo_Updated.txt"),
					new File("datatype-akp_Updated.txt"),
					ontology, true);
			
			onDatatype.readAKPs_Grezzo();
		}
		System.out.println(Duration.between(instant, Instant.now()).getSeconds() +"s" );
		
		System.out.println("START object property minimalization");
		instant = Instant.now();
		if(obj_grezzo.exists()) {
			PropertyMinimalizator onObject= new PropertyMinimalizator(new File(patterns_DirPath + "/object-akp_grezzo.txt"),
					new File("object-akp_grezzo_Updated.txt"),
					new File("object-akp_Updated.txt"),
					ontology, true);
			
			onObject.readAKPs_Grezzo();
		}
		System.out.println(Duration.between(instant, Instant.now()).getSeconds() +"s" );
	}
}

