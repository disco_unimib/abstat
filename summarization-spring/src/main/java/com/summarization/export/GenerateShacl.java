package com.summarization.export;

import java.io.File;
import java.io.FileNotFoundException;

import com.summarization.shacl.ShaclGenerator;

public class GenerateShacl {

	public static void main(String[] args) throws FileNotFoundException {
		
		String patternsPath = args[0];
		String outPath = args[1];
		
		// preparing dirs to save shapes
		File outDirs = new File(outPath);
		outDirs.mkdirs();
		ShaclGenerator sg = new ShaclGenerator(patternsPath, outPath);
		sg.generateShacl();

	}

}

