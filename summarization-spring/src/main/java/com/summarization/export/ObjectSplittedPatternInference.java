package com.summarization.export;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.time.Instant;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.summarization.dataset.ParallelProcessing;
import com.summarization.experiments.AKPsPartitioner;
import com.summarization.experiments.PatternGraphMerger;
import com.summarization.experiments.TriplesRetriever;
import com.summarization.export.Events;

public class ObjectSplittedPatternInference {
	
	private static void parallelProcessing(File specialParts_outputs, final PatternGraphMerger merger){
		ExecutorService executor = Executors.newFixedThreadPool(10);
		for( final File file : specialParts_outputs.listFiles()){
			if(file.getName().contains("_object")){
				executor.execute(new Runnable() {
					@Override
					public void run() {
						try {
							merger.process(file);
						} catch (Exception e) {
							Events.summarization().error(file, e);
						}
					}
				});
			}
		}
		executor.shutdown();
	    while(!executor.isTerminated()){}
	}
	
	
	private static void mergeFiles(File dir, String match, String finalFileName) throws Exception {
		for (File f : dir.listFiles()) {
			if (f.getName().contains(match)) {
				BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(f), StandardCharsets.UTF_8));
				BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(dir + "/" + finalFileName, true), StandardCharsets.UTF_8));
				String line = "";
				while ((line = br.readLine()) != null)
					if(!line.isEmpty())
						bw.write(line + "\n");
				br.close();
				bw.close();
				f.delete();
			}
		}
	}
	
	
	public static void main(String[] args) throws Exception{
		String akps_dir = args[0];
		File akps_Grezzo_splitted_dir = new File(args[1]);
		File ontology = new File(args[2]);
		File specialParts_outputs = new File(args[3]);
		File obj_grezzo = new File(akps_dir+"/object-akp_grezzo.txt");
		
		Instant instant = Instant.now();
		if(obj_grezzo.exists()) {			
			System.out.println("START Splitting");
			AKPsPartitioner splitter = new AKPsPartitioner(ontology);
			splitter.AKPs_Grezzo_partition(obj_grezzo, akps_Grezzo_splitted_dir, "_object");
			System.out.println(Duration.between(instant, Instant.now()).getSeconds() +"s" );
			
			//-----------------------------------------------------------      PatternGraph      -------------------------------------------------------------------------------		
			instant = Instant.now();
			System.out.println("START step 1");
			
			TriplesRetriever retriever = new TriplesRetriever(ontology, new File(akps_dir), akps_Grezzo_splitted_dir, specialParts_outputs);
			new ParallelProcessing(akps_Grezzo_splitted_dir, "_object.txt").process(retriever);
			retriever = null;   
			
			System.out.println(Duration.between(instant, Instant.now()).getSeconds() +"s" );

			//-----------------------------------------------------------     Special PGs Merge    -------------------------------------------------------------------------------
			instant = Instant.now();
			System.out.println("START step 2");
			
			PatternGraphMerger merger = new PatternGraphMerger(ontology, new File(akps_dir));
			
			//Dati n patterngraph SPECIALI(cioè con astrazione solo sui concetti) PGS1, PGS2,...PGSn. Dato l'insieme di predicati usati dai PG (uno per ogni PG) appartenenti alla stessa famiglia nel propertyGraph.
			//PGS1 PGS2...PGSn devono essere mergiati a livello "base", ovvero, no verranno modificati solo i pattern che usano topProperties  ma anche quelli ai livelli inferiori.
			//specialParts_outputs contiene m cartelle, ogni cartella contiene dei PGS da mergiare. Alla fine della chiamata che segue, avremo quindi m PG.
			parallelProcessing(specialParts_outputs, merger);
		    
			mergeFiles(new File(akps_dir),"HEADpatterns_object_unmerged_", "HEADpatterns_object_unmerged.txt");
			
			//ora che non abbiamo più PG speciali, ma tutti omegenei, possiamo fare il merge degli HEADpatterns (pattern con topPropteries), e ottenere un UNICO PG.
		    merger.mergeHeadPatterns("object");
		    
			mergeFiles(new File(akps_dir),"patterns_splitMode_object_", "patterns_splitMode_object.txt");
			
			System.out.println(Duration.between(instant, Instant.now()).getSeconds() +"s" );
		}		
	}
	
}

