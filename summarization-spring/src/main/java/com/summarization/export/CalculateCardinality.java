package com.summarization.export;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class CalculateCardinality {

	//metodo per il calcolo delle cardinalità
	public static void calcCardinality(File file, ArrayList<String> list, File resultFile, boolean richCardinalities) throws Exception{
		HashMap<String,Long> subject = new HashMap<String,Long>();
		HashMap<String,Long> object = new HashMap<String,Long>();
		BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file.getAbsolutePath()), StandardCharsets.UTF_8));
		String s;
		while ((s = br.readLine()) != null ){
			if (!(s.equals(""))){
				String subj = s.split("##")[0];
				String obj = s.split("##")[2];

				if(subject.containsKey(subj)){
					long value = subject.get(subj);
					subject.put(subj, value+1);
				}
				else
					subject.put(subj,(long) 1);

				if(object.containsKey(obj)){
					long value = object.get(obj);
					object.put(obj, value+1);
				}
				else
					object.put(obj, (long) 1);
			}
		}
		br.close();

		long[]cardinalities = new long[6];
		
		// soggetti distinti per un singolo oggetto
		Collection<Long> valuesO = new ArrayList<Long>();
		valuesO = object.values();
		cardinalities[0] = (long)Collections.max(valuesO);
		cardinalities[1] = avg(valuesO);
		cardinalities[2] = (long)Collections.min(valuesO);
		
		// oggetti  distinti per un singolo soggetto
		Collection<Long> valuesS = new ArrayList<Long>();
		valuesS = subject.values();
		cardinalities[3] = Collections.max(valuesS);
		cardinalities[4] = avg(valuesS);
		cardinalities[5] = Collections.min(valuesS);


		String fileName = file.getName();
		if(fileName.charAt(0)=='A')
			fileName = fileName.substring(3, fileName.length()-4);
		else
			fileName = fileName.substring(8, fileName.length()-4);
		int code = Integer.parseInt(fileName);

		//AKP or Property Max subjs-obj Avg subjs-obj Min subjs-obj Max subj-objs Avg subj-objs Min subj-objs
		String tmp = list.get(code)+"##"+cardinalities[0]+"##"+cardinalities[1]+"##"+cardinalities[2]+"##"+cardinalities[3]+"##"+cardinalities[4]+"##"+cardinalities[5];
		
		if(richCardinalities) {
			Map<Long, Long> subjsObjMap = new HashMap<Long,Long>();
			Map<Long, Long> subjObjsMap = new HashMap<Long,Long>();
			for(long n : valuesO) {
				long key = n;
				if(!subjsObjMap.containsKey(key))
					subjsObjMap.put(key, new Long(1));	
				else
					subjsObjMap.put(key, subjsObjMap.get(key)+1);
			}
			for(long n : valuesS) {
				long key = n;
				if(!subjObjsMap.containsKey(key))
					subjObjsMap.put(key, new Long(1));	
				else
					subjObjsMap.put(key, subjObjsMap.get(key)+1);
			}
			String subjsObjString = "[" + subjsObjMap + "]";
			String subjObjsString = "[" + subjObjsMap + "]";	
			
			//adding richCardinalities
			tmp += "##"+subjsObjString+"##"+subjObjsString;
		}
		
		OutputStreamWriter fos;
		fos = new OutputStreamWriter(new FileOutputStream(resultFile.getPath().substring(0,resultFile.getPath().length()-4 ) + "_" + UUID.randomUUID().toString() + ".txt", true), StandardCharsets.UTF_8);
		fos.write(tmp+"\n");
		fos.close();
	}


	//metodo per parallelizzare il lavoro in ogni file Property.txt e AKP.txt
	public static void concurrentWork(File folder, final ArrayList<String> list, final File resultFile, boolean richCardinalities) throws Exception{
		final ExecutorService executor = Executors.newFixedThreadPool(10);
		for(final File file : folder.listFiles()){
			executor.execute(new Runnable() {
				@Override
				public void run() {
					try {
						calcCardinality(file, list, resultFile, richCardinalities);
					} catch (Exception e) {
						Events.summarization().error(file, e);
					}
				}
			});
		}
		executor.shutdown();
		while(!executor.isTerminated()){}
	}

	public static long avg(Collection<Long> values){
		long n = 0;
		long tot = 0;
		for(long el : values){
			tot = tot+el;
			n++;
		}
		return tot/n;
	}

}
