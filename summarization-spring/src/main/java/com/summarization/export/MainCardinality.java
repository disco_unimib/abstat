package com.summarization.export;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;

import com.summarization.export.CalculateCardinality;
import com.summarization.export.Split;

public class MainCardinality {

	
	private static void mergeFiles(File dir, String match, String finalFileName) throws Exception {
		for (File f : dir.listFiles()) {
			if (f.getName().contains(match)) {
				BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(f), StandardCharsets.UTF_8));
				BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(dir + "/" + finalFileName, true), StandardCharsets.UTF_8));
				String line = "";
				while ((line = br.readLine()) != null)
					if(!line.isEmpty())
						bw.write(line + "\n");
				br.close();
				bw.close();
				f.delete();
			}
		}
	}
	
	
	public static void main(String[] args) throws Exception{
		String path = args[0];
		boolean richCardinalities = Boolean.valueOf(args[1]);

		File obj_grezzo = new File(path+"/object-akp_grezzo.txt");
		File dt_grezzo = new File(path+"/datatype-akp_grezzo.txt");
		ArrayList<String> listP = new ArrayList<String>();
		ArrayList<String> listAKP = new ArrayList<String>();

		Instant instant = Instant.now();
		
		if(obj_grezzo.exists()) {
			instant = Instant.now();
			System.out.println("START akp-obj grezzo Splitting");
			Split.readFromFiles(obj_grezzo.getAbsolutePath(), path, listP, listAKP);
			System.out.println(Duration.between(instant, Instant.now()).getSeconds() +"s" );
		}
		if(dt_grezzo.exists()) {
			instant = Instant.now();
			System.out.println("START akp-dt grezzo Splitting");
			Split.readFromFiles(dt_grezzo.getAbsolutePath(), path, listP, listAKP);
			System.out.println(Duration.between(instant, Instant.now()).getSeconds() +"s" );
		}
		
		File folderAkps = new File(path+"/Akps");
		File patternCard = new File(path+"/patternCardinalities.txt");
		
		System.out.println("START cardinalities computation");
		instant = Instant.now();
//		CalculateCardinality.concurrentWork(folderProps, listP, globalCard, richCardinalities );
		CalculateCardinality.concurrentWork(folderAkps, listAKP, patternCard, richCardinalities);
		System.out.println(Duration.between(instant, Instant.now()).getSeconds() +"s" );

		System.out.println("finishing");
		instant = Instant.now();
		mergeFiles(new File(path),"patternCardinalities_", "patternCardinalities.txt");
		
		//mappatura di akp+nome file di testo
		File mapAkps = new File(path+"/mapAkps.txt");
		OutputStreamWriter fos =  new OutputStreamWriter(new FileOutputStream(mapAkps.getPath(), true), StandardCharsets.UTF_8);
		for(String akp : listAKP){
			String index = Integer.toString(listAKP.indexOf(akp));
			String map = akp+" - AKP"+index+".txt";
			fos.write(map+"\n");
		}
		fos.close();
		System.out.println(Duration.between(instant, Instant.now()).getSeconds() +"s" );
	}

}
