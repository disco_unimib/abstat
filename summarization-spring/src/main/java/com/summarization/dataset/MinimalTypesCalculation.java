package com.summarization.dataset;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;

import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.summarization.ontology.ConceptExtractor;
import com.summarization.ontology.Concepts;
import com.summarization.ontology.OntologySubclassOfExtractor;
import com.summarization.ontology.Properties;
import com.summarization.ontology.PropertyExtractor;
import com.summarization.ontology.TypeGraph;

public class MinimalTypesCalculation implements Processing{

	private TypeGraph graph;
	private List<String> subclassRelations;
	private File targetDirectory;

	public MinimalTypesCalculation(OntModel ontology, File targetDirectory) throws Exception {
		Concepts concepts = extractConcepts(ontology);
		
		this.targetDirectory = targetDirectory;
		this.graph = new TypeGraph(concepts, subclassRelations);
	}

	@Override
	public void endProcessing() throws Exception {}
	
	@Override
	public void process(InputFile types) throws Exception {
		HashMap<String, Integer> conceptCounts = new HashMap<String, Integer>();
	//	List<String> externalConcepts = new ArrayList<String>();
		HashMap<String, HashSet<String>> minimalTypes = new HashMap<String, HashSet<String>>();
		
		while(types.hasNextLine()){
			String line = types.nextLine();
			try {
				String[] resources = line.split("##");
				String entity = resources[0];
				String concept = resources[2];
				trackConcept(entity, concept, conceptCounts);
				trackMinimalType(entity, concept, minimalTypes);
				
			}catch(Exception e) {
				e.printStackTrace();
				System.out.println(line);
			}
		}
		
		String prefix = new Files().prefixOf(types);
		writeConceptCounts(conceptCounts, targetDirectory, prefix);
		writeMinimalTypes(minimalTypes, targetDirectory, prefix);
	}

	private void trackMinimalType(String entity, String concept, HashMap<String, HashSet<String>> minimalTypes) {
		if(!minimalTypes.containsKey(entity)) minimalTypes.put(entity, new HashSet<String>());
		for(String minimalType : new HashSet<String>(minimalTypes.get(entity))){
			if(!graph.pathsBetween(minimalType, concept).isEmpty()){
				return;
			}
			if(!graph.pathsBetween(concept, minimalType).isEmpty()){
				minimalTypes.get(entity).remove(minimalType);
			}
		}
		minimalTypes.get(entity).add(concept);
	}

	private void trackConcept(String entity, String concept, HashMap<String, Integer> counts) {
		if(counts.containsKey(concept))	{
			counts.put(concept, counts.get(concept) + 1);
		}else{
			counts.put(concept, 1);
		}
	}

	private void writeConceptCounts(HashMap<String, Integer> conceptCounts, File directory, String prefix) throws Exception {
		BulkTextOutput countConceptFile = connectorTo(directory, prefix, "countConcepts");
		for(Entry<String, Integer> concept : conceptCounts.entrySet()){
			countConceptFile.writeLine(concept.getKey() + "##" + concept.getValue());
		}
		countConceptFile.close();
	}

	private void writeMinimalTypes(HashMap<String, HashSet<String>> minimalTypes, File directory, String prefix) throws Exception {
		BulkTextOutput connector = connectorTo(directory, prefix, "minType");
		for(Entry<String, HashSet<String>> entityTypes : minimalTypes.entrySet()){
			ArrayList<String> types = new ArrayList<String>(entityTypes.getValue());
			Collections.sort(types);
			connector.writeLine(types.size() + "##" + entityTypes.getKey() + "##" + StringUtils.join(types, "#-#"));
		}
		connector.close();
	}
	
	private BulkTextOutput connectorTo(File directory, String prefix, String name) {
		return new BulkTextOutput(new FileSystemConnector(new File(directory, prefix + "_" + name + ".txt")), 1000);
	}

	private Concepts extractConcepts(OntModel ontology) {
		PropertyExtractor pExtract = new PropertyExtractor();
		pExtract.setProperty(ontology);
		
		Properties properties = new Properties();
		properties.setProperty(pExtract.getProperty());
		properties.setExtractedProperty(pExtract.getExtractedProperty());
		properties.setCounter(pExtract.getCounter());
		
		ConceptExtractor cExtract = new ConceptExtractor();
		cExtract.setConcepts(ontology, true);
		
		Concepts concepts = new Concepts();
		concepts.setConcepts(cExtract.getConcepts());
		concepts.setExtractedConcepts(cExtract.getExtractedConcepts());
		concepts.setObtainedBy(cExtract.getObtainedBy());
		
		OntologySubclassOfExtractor extractor = new OntologySubclassOfExtractor();
		extractor.setConceptsSubclassOf(concepts, ontology);
		
		this.subclassRelations = new ArrayList<String>();
		for(List<OntClass> subClasses : extractor.getConceptsSubclassOf().getConceptsSubclassOf()){
			this.subclassRelations.add(subClasses.get(0) + "##" + subClasses.get(1));
		}
		
	//	OntologyDomainRangeExtractor DRExtractor = new OntologyDomainRangeExtractor();
	//	DRExtractor.setConceptsDomainRange(concepts, properties);
		return concepts;
	}
}

