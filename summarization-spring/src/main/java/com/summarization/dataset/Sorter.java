package com.summarization.dataset;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.time.Duration;
import java.time.Instant;

public class Sorter implements Processing {

	@Override
	public void process(InputFile file) throws Exception {
		Instant now = Instant.now();

		String cmd = "sort -u " + file.absoluteName() + " -o " + file.absoluteName();
		System.out.println("START " + file.name());
		Process p = Runtime.getRuntime().exec(cmd);
		BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
		String line = "";
		while ((line = reader.readLine()) != null)
			System.out.println(line);
		p.waitFor();

		System.out.println(file.name() + ": " + Duration.between(now, Instant.now()).getSeconds() +"s");
	}

	@Override
	public void endProcessing() throws Exception {
		// TODO Auto-generated method stub

	}

}
