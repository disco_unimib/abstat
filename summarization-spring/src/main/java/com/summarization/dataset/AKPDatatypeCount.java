package com.summarization.dataset;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

import com.summarization.export.Events;


public class AKPDatatypeCount implements NTripleAnalysis{

	private MinimalTypes types;
	private HashMap<String, Long> akps;
	private OutputStreamWriter fos;
	
	public AKPDatatypeCount(InputFile minimalTypes) throws Exception {
		this.types = new PartitionedMinimalTypes(minimalTypes);
		this.akps = new HashMap<String, Long>();
		fos = new OutputStreamWriter(new FileOutputStream("datatype-akp_grezzo_" + UUID.randomUUID().toString() + ".txt", true), StandardCharsets.UTF_8);
	}
	
	public HashMap<String, Long> counts() {
		return akps;
	}

	public AKPDatatypeCount track(NTriple triple) {
		String datatype = triple.dataType();
		String subject = triple.subject().toString();
		String property = triple.property().toString();
		
		ArrayList<String> AKPs = new ArrayList<String>();
		for(String type : types.of(subject)){
			String key = type + "##" + property + "##" + datatype;
			AKPs.add(key);	
			if(!akps.containsKey(key)) 
				akps.put(key, 0l);
			akps.put(key, akps.get(key) + 1);
		}

		write_akps_grezzo(subject, property, triple.object().toString(), AKPs.toString());

		return this;
	}
	
	
	public void write_akps_grezzo(String subject, String property, String object, String akpsList){
		try{
			String riga = "<"+subject+"##"+ property+"##"+ object.replaceAll("\\n", " ")+"> " + akpsList;
			riga +="\n";
			fos.write(riga);
		}
		catch(Exception e){
			Events.summarization().error("datatype-akp_grezzo.txt", e);
		}
	}
	
	public void finalize() throws IOException {
		fos.close();
	}
}