package com.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;

@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {
    @Autowired
    private ResourceServerTokenServices tokenServices;
    
    @Value("${restriction.policy}")
    private String policy;

    @Value("${security.jwt.resource-ids}")
    private String resourceIds;

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
        resources.resourceId(resourceIds).tokenServices(tokenServices);
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
    	if(policy.equals("full-open")) {
    		http
    		.requestMatchers()
            .and()
            .authorizeRequests()
            .antMatchers("/api/v1/**" ).permitAll()
            .antMatchers("/api/v2/**" ).permitAll();
    	}
    	else if(policy.equals("full-closed")) {
    		http
    		.requestMatchers()
            .and()
            .authorizeRequests()
            .antMatchers("/api/v1/**" ).authenticated()
            .antMatchers("/api/v2/**" ).authenticated();
    	}
    	else if(policy.equals("public")) {
    		http
    		.requestMatchers()
            .and()
            .authorizeRequests()
            .antMatchers("/api/v1/summarizator/**" ).authenticated()
            .antMatchers("/api/v1/submitconfig/**" ).authenticated()
            .antMatchers("/api/v1/dataset/delete/**" ).authenticated()
            .antMatchers("/api/v1/dataset/deleteDir/**" ).authenticated()
            .antMatchers("/api/v1/ontology/delete/**" ).authenticated()
            .antMatchers("/api/v1/ontology/deleteDir/**" ).authenticated()
            .antMatchers("/api/v1/upload/ds/**" ).authenticated()
            .antMatchers("/api/v1/upload/ont/**" ).authenticated()
            .antMatchers("/api/v/consolidate/**" ).authenticated();
    	}
    }
}
